//const fileClient = SolidFileClient;
const fileClient = SolidFileClient;

let pathchange = true;
let messagechange;

let firstActiveTab = null; //It has been created to prevent extension opening in the multiple tabs
const RenarrationSolidMainFolderVariables = {
  podMainRenarrationFolder: "renarration",
  podRenarrationFolderPath: "RenarrationFiles",
  podResponseFolderPath: "ResponseFiles",
  podFollowFolderPath: "FollowingFiles",
  podFollowingListPath: "FollowingList.ttl",
  podExtensionActivities: "ExtensionActivities.ttl"
};
const RenarrationSolidVariables = {
  UrlOfTheRenarrationSocialOntology: "http://soslab.cmpe.boun.edu.tr/ontologies/rnsoc.owl#",
  locationOfRenarrationFolderInTheSolidPodOfUser: "/" + RenarrationSolidMainFolderVariables.podMainRenarrationFolder + "/" + RenarrationSolidMainFolderVariables.podRenarrationFolderPath + "/",
  locationOfResponseFolderInTheSolidPodOfUserRSPCNTTO: "RSPCNTTO",
  locationOfRenarrationFolderInTheSolidPodOfUserRNCNT: "RNCNT-",
  locationOfFollowingListInTheSolidPodOfUser: "/" + RenarrationSolidMainFolderVariables.podMainRenarrationFolder + "/" + RenarrationSolidMainFolderVariables.podFollowFolderPath + "/" + RenarrationSolidMainFolderVariables.podFollowingListPath,
  locationOfExtensionActivitiesListInTheSolidPodOfUser: "/" + RenarrationSolidMainFolderVariables.podMainRenarrationFolder + "/" + RenarrationSolidMainFolderVariables.podExtensionActivities,
  urlOfRenarrationExtensionPod: "https://rnex-central.inrupt.net",
  locationOfRenarrationListInTheSolidPodOfRnEx: "/renarration/RenarrationList.ttl",
  locationOfResponseListInTheSolidPodOfRnEx: "/renarration/ResponseList.ttl",
  limitOfRenarrationsThatWillBeGotFromRenarratorsPod: 5,
  limitOfRenarrationsThatWillBeGotFromOtherRenarratorsPod: 4,
  limitOfRenarrationsThatWillBeGotForTheReadingMode: 4,
  limitOfResponsesThatWillBeGotForTheResponseMode: 5,
  textOfProfileCardMe: "/profile/card#me",
  renarrationFileType: ".ttl",
  getAllTheResponsesForTheNotification: "getAllTheResponsesForTheNotification",
  getAllTheRenarrationsForTheNotification: "getAllTheRenarrationsForTheNotification",
  objectExtensionActivityLastViewTimeOfNotification: "LastViewTimeOfNotification",
};


var originalRemoveItem = localStorage.removeItem;
localStorage.removeItem = function() {
  document.createEvent('Event').initEvent('itemRemoved', true, true);
  originalRemoveItem.apply(this, arguments);
  console.log("originalRemoveItem");
};
var originalSetItem = localStorage.setItem;
localStorage.setItem = function() {
  document.createEvent('Event').initEvent('itemInserted', true, true);
  originalSetItem.apply(this, arguments);
  console.log("originalSetItem");

};

var originalSetItem2 = localStorage.setItem;

localStorage.setItem = function(key, value) {
  var event = new Event('itemInserted');

  event.value = value; // Optional..
  event.key = key; // Optional..

  document.dispatchEvent(event);

  originalSetItem2.apply(this, arguments);
};

var localStorageSetHandler = function(e) {
  console.log('localStorage.set("' + e.key + '", "' + e.value + '") was called');

  if (e.key === "solid-auth-client") {
    if (firstActiveTab) {
      chrome.tabs.sendMessage(firstActiveTab.id, {
        "message": "refresh_the_page"
      });
      console.log("refresh the page");
    }
  }
};

document.addEventListener("itemInserted", localStorageSetHandler, false);



window.addEventListener('storage', function(e) {
  console.log("here is the storage ", e);

});

let chrome_storage = {
  rn_extension_clicked_and_working: function(val) {
    chrome.storage.local.set({
      rn_extension_clicked_and_working: val
    });
  },
  rn_extension_current_tab_url: function(val) {
    chrome.storage.local.set({
      rn_extension_current_tab_url: val
    });
  },
  rn_set_the_renarrators_info: async function(val) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              renarrators_profile_info: val
            }
          }, function() {
            resolve("nice1 rn_set_the_renarrators_info");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.renarrators_profile_info = val;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_set_the_renarrators_info");

          });
        }
      });
    });
    return await a_promise;
  },
  rn_set_the_current_session_renarrators_info: function(val) {
    chrome.storage.local.set({
      rn_session_renarrators_info: val
    });
  },
  rn_set_the_renarrations_and_the_table_for_the_renarrator_account_mode: async function(success) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["account_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            account_mode: {
              renarrations_and_the_table: success
            }
          }, function() {
            resolve("nice1 rn_set_the_renarration_documents_array_for_the_renarrator_account_mode");

          });
        } else {
          let createdRenarrationObject = value.account_mode;
          createdRenarrationObject.renarrations_and_the_table = success;

          chrome.storage.local.set({
            account_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_set_the_renarration_documents_array_for_the_renarrator_account_mode");

          });
        }
      });

    });
    return await a_promise;
  },
  rn_set_the_renarrations_and_the_table_for_the_reading_mode: async function(success) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["reading_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            reading_mode: {
              renarrations_and_the_table: success
            }
          }, function() {
            resolve("nice1 rn_set_the_renarration_documents_array_for_the_renarrator_reading_mode");

          });
        } else {
          let createdRenarrationObject = value.reading_mode;
          createdRenarrationObject.renarrations_and_the_table = success;

          chrome.storage.local.set({
            reading_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_set_the_renarration_documents_array_for_the_renarrator_reading_mode");

          });
        }
      });

    });
    return await a_promise;
  },
  rn_set_the_renarrations_and_the_table_for_the_profile_mode: async function(success) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              renarrations_and_the_table: success
            }
          }, function() {
            resolve("nice1 rn_set_the_renarration_documents_array_for_the_renarrator_profile_mode");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.renarrations_and_the_table = success;

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_set_the_renarration_documents_array_for_the_renarrator_profile_mode");

          });
        }
      });

    });
    return await a_promise;
  },
  rn_set_the_renarration_documents_array_for_the_renarrator_profile_mode: async function(success) {
    let a_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get(["profile_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            profile_mode: {
              renarration_documents_array: success.slice()
            }
          }, function() {
            resolve("nice1 rn_set_the_renarration_documents_array_for_the_renarrator_account_mode");

          });
        } else {
          let createdRenarrationObject = value.profile_mode;
          createdRenarrationObject.renarration_documents_array = success.slice();

          chrome.storage.local.set({
            profile_mode: createdRenarrationObject
          }, function() {
            resolve("nice2 rn_set_the_renarration_documents_array_for_the_renarrator_account_mode");

          });
        }
      });

    });
    return await a_promise;
  },
  rn_set_the_session_info: function(val) {

    chrome.storage.local.set({
      rn_session_info: val
    });
  },
  rn_get_the_session_info: async function() {
    let current_mode_promise = new Promise((resolve, reject) => {
      chrome.storage.local.get("rn_session_info", function(val) {
        if (val.rn_session_info === undefined || val.rn_session_info === null) {
          reject(null);
        } else {
          resolve(val.rn_session_info);
        }
      });
    });
    return await current_mode_promise;
  },
  rn_set_response_mode_response_array: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["response_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            response_mode: {
              response_documents_array: success.slice()
            }
          });
        } else {
          let createdRenarrationObject = value.response_mode;
          createdRenarrationObject.response_documents_array = success.slice();

          chrome.storage.local.set({
            response_mode: createdRenarrationObject
          });

        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_notification_mode_notification_array: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["notification_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            notification_mode: {
              notification_renarrations_array: success.renarrationList.slice(),
              notification_response_array: success.responseList.slice()
            }
          });
        } else {
          let createdNotificationObject = value.notification_mode;
          createdNotificationObject.notification_renarrations_array = success.renarrationList.slice();
          createdNotificationObject.notification_response_array = success.responseList.slice();
          chrome.storage.local.set({
            notification_mode: createdNotificationObject
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_recommending_mode_recommendation_depending_on_second_degree_response_array: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              recommendation_second_degree_response_array: success.renarrationArrayFinalObject
            }
          });
        } else {
          let createdNotificationObject = value.recommending_mode;
          createdNotificationObject.recommendation_second_degree_response_array = success.renarrationArrayFinalObject;
          chrome.storage.local.set({
            recommending_mode: createdNotificationObject
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_recommending_mode_search_operation_all_the_renarrations_array: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              search_operation_all_the_renarrations_array: success.renarrationStoreArrayMinimized,
              search_operation_all_the_renarrations_initial_array: success.renarrationStoreArray
            }
          });
        } else {
          let createdNotificationObject = value.recommending_mode;
          createdNotificationObject.search_operation_all_the_renarrations_array = success.renarrationStoreArrayMinimized;
          createdNotificationObject.search_operation_all_the_renarrations_initial_array = success.renarrationStoreArray;
          chrome.storage.local.set({
            recommending_mode: createdNotificationObject
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_recommending_mode_search_operation_filtered_rnex_renarrations_array: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              search_operation_filtered_rnex_renarrations_array: success
            }
          }, (success) => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.recommending_mode;
          createdNotificationObject.search_operation_filtered_rnex_renarrations_array = success;
          chrome.storage.local.set({
            recommending_mode: createdNotificationObject
          }, (success) => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_recommending_mode_chosen_renarration: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["recommending_mode"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            recommending_mode: {
              chosen_renarration: success
            }
          }, (success) => {
            resolve(success);
          });
        } else {
          let createdRenarrationObject = value.recommending_mode;
          createdRenarrationObject.chosen_renarration = success;

          chrome.storage.local.set({
            recommending_mode: createdRenarrationObject
          }, (success) => {
            resolve(success);
          });

        }
      });

    });
    return await current_mode_promise;
  },
  rn_set_external_api_analytics_for_the_applied_renarration_on_the_page: async function(success) {
    let current_mode_promise = new Promise((resolve, reject) => {

      chrome.storage.local.get(["rn_external_analytics_result_for_applied_renarration"], function(value) {
        if (Object.keys(value).length === 0) {
          chrome.storage.local.set({
            rn_external_analytics_result_for_applied_renarration: {
              topics_extraction: success
            }
          }, () => {
            resolve(success);
          });
        } else {
          let createdNotificationObject = value.rn_external_analytics_result_for_applied_renarration;
          createdNotificationObject.topics_extraction = success;
          chrome.storage.local.set({
            rn_external_analytics_result_for_applied_renarration: createdNotificationObject
          }, () => {
            resolve(success);
          });
        }
      });

    });
    return await current_mode_promise;
  }
};


chrome.browserAction.onClicked.addListener(function(tab) {
  let path;
  let icon1_path = {
    path: {
      "16": "img/renarration.png"
    }
  };
  let icon2_path = {
    path: {
      "16": "img/renarration2.png"
    }
  };
  chrome_storage.rn_extension_clicked_and_working(pathchange);
  if (pathchange) {

    path = icon1_path;
    messagechange = "clicked_browser_action";
    pathchange = false;
  } else {
    path = icon2_path;
    messagechange = "cancel_clicked_browser_action";
    pathchange = true;
  }
  chrome.browserAction.setIcon(path, () => {

  });

  chrome.tabs.query({
    active: true,
    currentWindow: true
  }, function(tabs) {
    firstActiveTab = tabs[0];
    if (firstActiveTab) {
      chrome.tabs.sendMessage(firstActiveTab.id, {
        "message": messagechange
      });
    }
  });

});


chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {

  chrome_storage.rn_extension_current_tab_url(tab.url);
  if (firstActiveTab && firstActiveTab.id) {
    if (firstActiveTab.id === tab.id) {
      let message = "clicked_browser_action";
      if (pathchange) {
        message = "cancel_clicked_browser_action";
      }
      if (firstActiveTab) {
        chrome.tabs.sendMessage(firstActiveTab.id, {
          "message": message
        });
      }
    }
  }


});

const popupUri = chrome.runtime.getURL("login-popup.html");

const ExternalAPIOperations = {
  settings: {
    origin_url: "https://api.meaningcloud.com/topics-2.0",
    meaningcloud_key: "d1cb0149b06c352db714e201cd5cb594",
    lang: 'en',
    topic_types: {
      named_entities: "e",
      concepts: "c",
      time_expressions: "t",
      money_expressions: "m",
      quantity_expressions_beta: "n",
      other_expressions: "o",
      quotations: "q",
      relations: "r",
      all: "a"
    },
    unknown_words: {
      enabled: "y",
      disabled: "n"
    }
  },
  json_settings_post: function(content_text = '') {
    let json_post = {
      key: ExternalAPIOperations.settings.meaningcloud_key,
      of: "json",
      lang: ExternalAPIOperations.settings.lang,
      ilang: ExternalAPIOperations.settings.lang,
      txt: content_text,
      tt: `${ExternalAPIOperations.settings.topic_types.named_entities}${ExternalAPIOperations.settings.topic_types.concepts}`,
      uw: ExternalAPIOperations.settings.unknown_words.enabled
    };
    return json_post;
  },
  postData: async function(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "Accept": "application/json"
      },
      body: JSON.stringify(data)
    });
    return response.json();
  },
  sendTheTextToExternalAPI: async function(textToAnalyze) {
    let json_post = ExternalAPIOperations.json_settings_post(textToAnalyze);

    const result = await ExternalAPIOperations.postData(ExternalAPIOperations.settings.origin_url, json_post);

    return result;
  }
};

const SolidPodOperations = {
  solidLogin: async function() {
    const loginPromise = new Promise((resolve, reject) => {

      solid.auth.popupLogin({
        popupUri
      }).then(success => {
        resolve("Login is successfull", success);
      }).catch(error => {
        reject("Some error has occured while logging in", error);
      });

    });


    return await loginPromise;
  },
  solidlogin3: async function() {
    const loginPromise = new Promise((resolve, reject) => {
      fileClient.popupLogin().then(webId => {
        renarrator = webId;
        console.log("renarrator webid is" + renarrator);
        console.log(`Logged in as ${webId}.`);
        resolve(webId);
      }).catch(err => {
        console.log(err);
        reject(err);
      });
    });
    return await loginPromise;
  },
  solidLogout: async function() {
    const logoutPromise = new Promise((resolve, reject) => {

      solid.auth.logout().then(success => {
        localStorage.removeItem('solid-auth-client');
        chrome.storage.local.clear(() => {
          console.log("chrome.storage.local has been cleared after the logout");
        });
        resolve("Logout is successfull", success);
      }).catch(error => {
        reject("Some error has occured while logging out", error);
      });



    });


    return await logoutPromise;
  },
  solidTrackSession: async function() {


    const sessionPromise = new Promise((resolve, reject) => {
      solid.auth.trackSession(s => {
        chrome_storage.rn_set_the_session_info(s);
        console.log("success", s);
        resolve(s);
      }).catch(e => {
        chrome_storage.rn_set_the_session_info(null);
        console.log("error", e);
        reject(e);
      });
    });
    return await sessionPromise;

  },
  solidTrackSessionWithLocalStorage: function() {
    if (localStorage['solid-auth-client'] !== undefined && localStorage['solid-auth-client'] !== null) {
      let localStorageSolidAuthClient = JSON.parse(localStorage['solid-auth-client']);

      if (localStorageSolidAuthClient.session !== undefined && localStorageSolidAuthClient.session !== null) {
        let webIdWithAndWithoutProfileCard = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(localStorageSolidAuthClient.session.webId);
        let webIdWithoutProfileCard = webIdWithAndWithoutProfileCard[0];
        let sessionWebidAndIdp = {
          webIdWithoutProfileCard: webIdWithoutProfileCard,
          webId: localStorageSolidAuthClient.session.webId,
          idp: localStorageSolidAuthClient.session.idp
        };
        chrome_storage.rn_set_the_session_info(sessionWebidAndIdp);
        return sessionWebidAndIdp;
      } else if (localStorageSolidAuthClient.rpConfig !== undefined && localStorageSolidAuthClient.rpConfig !== null) {
        let webIdWithAndWithoutProfileCard = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(localStorageSolidAuthClient.rpConfig.provider.url);
        let webIdWithoutProfileCard = webIdWithAndWithoutProfileCard[0];
        let sessionWebidAndIdp = {
          webIdWithoutProfileCard: webIdWithoutProfileCard,
          webId: webIdWithAndWithoutProfileCard[1],
          idp: localStorageSolidAuthClient.rpConfig.provider.configuration.issuer
        };
        chrome_storage.rn_set_the_session_info(sessionWebidAndIdp);
        return sessionWebidAndIdp;
      } else {
        chrome_storage.rn_set_the_session_info(null);
        console.log("Error in solidTrackSessionWithLocalStorage, solid-auth-client exist but session doesn't exist you should login");
        return undefined;
      }

    } else {
      chrome_storage.rn_set_the_session_info(null);
      console.log("Error in solidTrackSessionWithLocalStorage, solid-auth-client doesn't exist you should login");
      return undefined;
    }
  },
  getTheProfileInfo: async function(renarrators_pod_related_url) {
    let get_the_renarrator_webId_from_the_url_with_regex = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(renarrators_pod_related_url);
    let renarrator_profile_card = get_the_renarrator_webId_from_the_url_with_regex[1];
    let renarrator_webId = get_the_renarrator_webId_from_the_url_with_regex[0];
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);
    const VCARD = new $rdf.Namespace('http://www.w3.org/2006/vcard/ns#');
    const FOAF = $rdf.Namespace('http://xmlns.com/foaf/0.1/');

    const a_promise = new Promise((resolve, reject) => {

      let reninf = {
        name: null,
        role: null,
        email: null,
        picture: null,
        friends: null,
        webId: renarrator_webId
      };
      let me = $rdf.sym(renarrator_profile_card);

      fetcher.load(me).then(response => {

        let name = store.any(me, VCARD('fn'));
        if (name) {
          reninf.name = name.value;
        }

        let role = store.any(me, VCARD('role'));
        if (role) {
          reninf.role = role.value;
        }

        let email = store.any(me, VCARD('hasEmail'));
        if (email) {
          let emailpart2 = store.any($rdf.sym(email.value), VCARD('value'));
          if (emailpart2) {
            let mailtopart = emailpart2.value.substr(0, 7);
            if (mailtopart === "mailto:") {
              reninf.email = emailpart2.value.substr(7);
            } else {
              reninf.email = emailpart2.value;
            }
          }
        }

        let pic = store.any(me, VCARD('hasPhoto'));
        if (pic) {
          reninf.picture = pic.value;
        }
        let friends = store.each(me, FOAF('knows'), undefined);
        if (friends) {
          let friends_array = [];
          friends.forEach((friend) => {
            friends_array.push(friend.value);
          });
          reninf.friends = friends_array;
        }
        resolve(reninf);
      }).catch(e => reject(e));
    });

    return await a_promise;

  },
  readTheFile: async function readTheFile(location) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    return await fetcher.load(location);
  },
  getRenarrationFileFromRenarratorsPod: async function(locationurl) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);
    const renarrationReadPromise = new Promise((resolve, reject) => {

      fetcher.load(locationurl).then(res => {

        let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
        let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
        let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
        let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
        let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");


        let renarration_information = {
          // profile_card: null,
          // webId: null,
          // webId_label: null,
          // idp: null,
          // role: null,
          // email: null,
          name: null,
          motivation: null,
          creation_time: null,
          // unique_id: null,
          source_url: null,
          renarration_transformation: {
            // count: 0,
            date: [],
            xpath: [],
            action: [],
            language: [],
            content: []
          }
        };

        renarration_information.renarration_transformation.count = store.match(null, CNT('content'), null, null).map(st => st.object.value).length;

        renarration_information.renarration_transformation.content = store.match(null, CNT('content'), null, null).map(st => st.object.value).slice();
        renarration_information.renarration_transformation.xpath = store.match(null, RDF('value'), null, null).map(st => st.object.value).slice();
        renarration_information.renarration_transformation.date = store.match(null, RN('createdAt'), null, null).map(st => st.object.value).slice();
        renarration_information.renarration_transformation.action = store.match(null, RN('actionOnDocument'), null, null).map(st => st.object.value).slice();
        renarration_information.renarration_transformation.language = store.match(null, PURL('language'), null, null).map(st => st.object.value).slice();

        renarration_information.source_url = store.match(null, RN('onSourceDocument'), null, null).map(st => st.object.value);
        renarration_information.creation_time = store.match(null, RN('renarratedAt'), null, null).map(st => st.object.value);
        renarration_information.name = store.match(null, RN('name'), null, null).map(st => st.object.value).slice();
        renarration_information.motivation = store.match(null, RN('hasMotivation'), null, null).map(st => st.object.value);

        console.log("renarration information", renarration_information);
        resolve(renarration_information);

      }).catch(err => {
        console.log("failed fetcher load", err);
        reject(err);
      });

    });

    return await renarrationReadPromise;
  },
  createTheEmptyFile: async function(location) {
    //const store = $rdf.graph();
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    const updater = new $rdf.UpdateManager();
    const creationPromise = new Promise((resolve, reject) => {
      fetcher.load(location).then(success => {
        //  console.log(success);
      }).catch(e => {
        updater.put($rdf.sym(location), [], 'text/turtle', (_url, success, message) => {
          if (success) {
            //    console.log(error);
            resolve(`The file in the ${location} location is created`);
          } else {
            reject(new Error(message));
          }
        });
      });
    });
    return await creationPromise;
  },
  createAclFile: function() {
    function aclRenarration(location) {
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let ACL = $rdf.Namespace("http://www.w3.org/ns/auth/acl#");
      let renSign = $rdf.Namespace(renarratorWebID.webIdWithoutProfileCard + "/renarrationTest2/");
      let currentLocationSign = $rdf.Namespace(location + "#");

      const additions = [];
      const deletions = [];
      const locationurl = location;
      const location_url_of_renarration_with_id = location + "#" + "Read";

      let file = $rdf.sym(location_url_of_renarration_with_id);

      additions.push($rdf.st(currentLocationSign('Read'), RDF('type'), ACL('Authorization'), $rdf.sym(locationurl)));

      additions.push($rdf.st(currentLocationSign('Read'), ACL('accessTo'), renSign(''), $rdf.sym(locationurl)));

      additions.push($rdf.st(currentLocationSign('Read'), ACL('agentClass'), ACL('AuthenticatedAgent'), $rdf.sym(locationurl)));

      additions.push($rdf.st(currentLocationSign('Read'), ACL('default'), renSign(''), $rdf.sym(locationurl)));

      additions.push($rdf.st(currentLocationSign('Read'), ACL('mode'), ACL('Read'), $rdf.sym(locationurl)));


      const updater = new $rdf.UpdateManager();
      updater.update(deletions, additions, (uri, ok, errormessage) => {
        console.log("inside uri, ok, errormessage", uri, ok, errormessage);
        console.log("inside createEmptyFileIn then part", location, additions, deletions);
        if (ok) {
          resolve(`The file on the ${location} location is updated`);
        } else {
          reject(errormessage);
        }
      });
    }

    aclRenarration(renarratorWebID.webIdWithoutProfileCard + "/renarrationTest2/.acl");
  },
  initialCreateRenarrationFolderAndFiles: async function(renarratorWebID, podMainRenarrationFolderPath, podRenarrationFolderPath, podResponseFolderPath, podFollowFolderPath, podFollowingListPath) {

    let webId = renarratorWebID.webIdWithoutProfileCard;
    let renarrationContainerName = podMainRenarrationFolderPath;

    async function createEmptyFolderOn(location, folderName) {
      let locationWithSlash = location + "/";
      let locationWholePath = location + "/" + folderName;
      const folderCreate = new Promise((resolve, reject) => {
        const store = $rdf.graph();
        const fetcher = new $rdf.Fetcher(store);

        fetcher.load(locationWholePath).then(success => {
          resolve(`The file on the ${locationWholePath} location already exists`);
        }).catch(e => {

          solid.auth.fetch(locationWithSlash, {
            method: 'POST', // or 'PUT'
            headers: {
              'Content-Type': 'text/turtle',
              'Link': '<http://www.w3.org/ns/ldp#BasicContainer>; rel="type"',
              'Slug': folderName
            }
          }).then(() => {
            resolve(`Success:The container ${folderName} on the ${location} location is created`);
          }).catch(() => {
            reject(new Error(message + `Error:The container ${folderName} on the ${location} location is not created`));
          });

        });


      });

      return await folderCreate;
    }

    async function createEmptyFileIn(location) {

      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const updater = new $rdf.UpdateManager();
      const creationPromise = new Promise((resolve, reject) => {
        fetcher.load(location).then(success => {
          resolve(`The file on the ${location} location already exists`);
        }).catch(e => {
          updater.put($rdf.sym(location), [], 'text/turtle', (_url, success, message) => {
            if (success) {
              resolve(`Success:The file on the ${location} location is created`);
            } else {
              reject(new Error(message + `Error:The file on the ${location} location is not created`));
            }
          });
        });
      });
      return await creationPromise;
    }
    async function initiateTheCreationOfFoldersOperations(location) {
      const initiatePromise = new Promise((resolve, reject) => {
        createEmptyFolderOn(webId, renarrationContainerName).then(() => {
          let locationPath = webId + "/" + renarrationContainerName;
          let renarrationFolderPath = podRenarrationFolderPath;
          let responseFolderPath = podResponseFolderPath;
          let followFolderPath = podFollowFolderPath;
          let followFilesPathList = locationPath + "/" + followFolderPath + "/" + podFollowingListPath;

          const promise1 = createEmptyFolderOn(locationPath, renarrationFolderPath).then((success) => {
            console.log(success);
          }).catch((error) => {
            console.log(error);
          });

          const promise2 = createEmptyFolderOn(locationPath, followFolderPath).then((success) => {
            console.log(success);
            createEmptyFileIn(followFilesPathList).then((success) => {
              console.log(success);
            }).catch((error) => {
              console.log(error);
            });

          }).catch((error) => {
            console.log(error);
          });

          const promise3 = createEmptyFolderOn(locationPath, responseFolderPath).then((success) => {
            console.log(success);
          }).catch((error) => {
            console.log(error);
          });

          const promises = [promise1, promise2, promise3];

          return Promise.allSettled(promises).
          then((results) => {
            results.forEach((result) => console.log(result.status));
            resolve("Success: initiateTheCreationOfFoldersOperations is done ");
          });




        }).catch((error) => {
          reject(error + "error occurred while creating the folders");
          console.log(error);
        });
      });
      return await initiatePromise;
    }

    return await initiateTheCreationOfFoldersOperations;
  },
  checkCreateAndUpdateTheFile: async function(renarratorWebIDObj, location, additions, deletions) {

    let location_save = location;
    let additions_save = additions.slice();
    let deletions_save = deletions.slice();

    async function createEmptyFileIn(location) {

      console.log("inside createEmptyFileIn", location, additions, deletions);
      //const store = $rdf.graph();
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const updater = new $rdf.UpdateManager();
      const creationPromise = new Promise((resolve, reject) => {
        fetcher.load(location).then(success => {
          resolve(`The file on the ${location} location already exists`);
        }).catch(e => {
          updater.put($rdf.sym(location), [], 'text/turtle', (_url, success, message) => {
            if (success) {
              resolve(`The file on the ${location} location is created`);
            } else {
              reject(new Error(message));
            }
          });
        });
      });
      return await creationPromise;
    }

    const updater = new $rdf.UpdateManager();
    const updatePromise = new Promise((resolve, reject) => {

      createEmptyFileIn(location).then(success => {
        updater.update(deletions, additions, (uri, ok, errormessage) => {
          console.log("inside createEmptyFileIn then part", location, additions, deletions);
          if (ok) {
            resolve(`The file on the ${location} location is updated`);
          } else {
            reject(errormessage);
          }
        });
      }).catch(error => {
        reject(error);
      });




    });

    return await updatePromise;
  },
  createAndUpdateTheFile: async function(location, additions, deletions) {

    let location_save = location;
    let additions_save = additions.slice();
    let deletions_save = deletions.slice();

    async function createEmptyFileIn(location) {

      console.log("inside createEmptyFileIn", location, additions, deletions);
      //const store = $rdf.graph();
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const updater = new $rdf.UpdateManager();
      const creationPromise = new Promise((resolve, reject) => {
        fetcher.load(location).then(success => {
          resolve(`The file on the ${location} location already exists`);
        }).catch(e => {
          updater.put($rdf.sym(location), [], 'text/turtle', (_url, success, message) => {
            if (success) {
              resolve(`The file on the ${location} location is created`);
            } else {
              reject(new Error(message));
            }
          });
        });
      });
      return await creationPromise;
    }

    const updater = new $rdf.UpdateManager();
    const updatePromise = new Promise((resolve, reject) => {

      createEmptyFileIn(location).then(success => {
        updater.update(deletions, additions, (uri, ok, errormessage) => {
          console.log("inside createEmptyFileIn then part", location, additions, deletions);
          if (ok) {
            resolve(`The file on the ${location} location is updated`);
          } else {
            reject(errormessage);
          }
        });
      }).catch(error => {
        reject(error);
      });

    });

    return await updatePromise;
  },
  deleteTheFile: async function(location) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);
    const deletePromise = new Promise((resolve, reject) => {
      fetcher.webOperation('DELETE', location).then(s => {
        resolve(`The file in the ${location} location is deleted`, s);
      }).catch(e => {
        reject(`The file in the ${location} location couldn't be deleted`, e);
      });
    });

    return await deletePromise;
  },
  renarration_turtle_operations: function(location, renarration_information_send_object, renarration_transformation_send_object) {
    console.log("renarration_turtle_operations renarration_information_send_object", renarration_information_send_object, renarration_transformation_send_object);
    let renarrator_name = renarration_information_send_object.name;
    let renarrator_webId = renarration_information_send_object.webId;
    let renarrator_role = renarration_information_send_object.role;
    let renarrator_email = renarration_information_send_object.email;
    let renarration_motivation = renarration_information_send_object.motivation;
    let renarration_source_url = renarration_information_send_object.source_url;
    let renarration_creation_time = renarration_information_send_object.creation_time;
    let renarration_unique_id = renarration_information_send_object.unique_id;

    let renarration_transformation_count = renarration_transformation_send_object.text_contents.count;
    let renarration_transformation_xpath = renarration_transformation_send_object.text_contents.xpath;
    let renarration_transformation_insert_prev_xpath = renarration_transformation_send_object.text_contents.insert_prev_xpath;
    let renarration_transformation_insert_next_xpath = renarration_transformation_send_object.text_contents.insert_next_xpath;
    let renarration_transformation_action = renarration_transformation_send_object.text_contents.action;
    let renarration_transformation_insert_value = renarration_transformation_send_object.text_contents.insert_value;
    let renarration_transformation_language = renarration_transformation_send_object.text_contents.language;
    let renarration_transformation_old_content = renarration_transformation_send_object.text_contents.old;
    let renarration_transformation_new_content = renarration_transformation_send_object.text_contents.new;
    let renarration_transformation_date = renarration_transformation_send_object.text_contents.date;
    let renarration_transformation_media_type = renarration_transformation_send_object.text_contents.media_type;

    let store = $rdf.graph();
    const additions = [];
    const deletions = [];
    const locationurl = location;
    const location_url_of_renarration_with_id = location + "#" + renarration_unique_id;
    let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
    let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
    let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
    let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
    let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
    let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
    let OA = $rdf.Namespace("http://www.w3.org/ns/oa#");

    const profile_card_me = $rdf.sym(renarrator_webId + RenarrationSolidVariables.textOfProfileCardMe);

    let blanknode1 = $rdf.blankNode();
    let blanknodes = [];
    for (let i = 0; i < renarration_transformation_count * 5; i++) {
      blanknodes[i] = $rdf.blankNode();
    }

    let file = $rdf.sym(location_url_of_renarration_with_id);
    additions.push($rdf.st(file, RDF('type'), OWL('NamedIndividual'), $rdf.sym(locationurl)));
    additions.push($rdf.st(file, RDF('type'), RN('Renarration'), $rdf.sym(locationurl)));
    additions.push($rdf.st(file, RN('renarratedBy'), profile_card_me, $rdf.sym(locationurl)));

    Object.entries(renarration_motivation).forEach((entry) => {
      if (entry[1]) {
        additions.push($rdf.st(file, RN('hasMotivation'), $rdf.lit(entry[0], '', XSD('string')), $rdf.sym(locationurl)));
      }

    });

    additions.push($rdf.st(file, RN('onSourceDocument'), $rdf.sym(renarration_source_url), $rdf.sym(locationurl)));


    for (let j = 0; j < renarration_transformation_count; j++) {
      let i = j * 5;

      if (renarration_transformation_action[j] === "insert") {
        if (renarration_transformation_insert_value[j] === "before") {
          additions.push($rdf.st(file, RN('hasRenarrationTransformation'), blanknodes[i], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], OA('hasSelector'), blanknodes[i + 1], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 1], RDF('type'), RN('RangeSelector'), $rdf.sym(locationurl)));


          additions.push($rdf.st(blanknodes[i + 1], OA('hasStartSelector'), blanknodes[i + 3], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 3], RDF('type'), OA('XpathSelector'), $rdf.sym(locationurl)));

          if (renarration_transformation_insert_prev_xpath[j] !== null) {
            additions.push($rdf.st(blanknodes[i + 3], RDF('value'), renarration_transformation_insert_prev_xpath[j], $rdf.sym(locationurl)));
          }

          additions.push($rdf.st(blanknodes[i + 1], OA('hasEndSelector'), blanknodes[i + 4], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 4], RDF('type'), OA('XpathSelector'), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 4], RDF('value'), renarration_transformation_xpath[j], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i], RN('createdAt'), $rdf.lit(renarration_transformation_date[j], '', XSD('dateTime')), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RN('hasAction'), $rdf.lit(renarration_transformation_action[j], '', XSD('string')), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RN('hasNarration'), blanknodes[i + 2], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 2], RDF('value'), renarration_transformation_new_content[j], $rdf.sym(locationurl)));


          additions.push($rdf.st(blanknodes[i + 2], RDF('type'), RN(renarration_transformation_media_type[j]), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RDF('type'), RN('RenarrationTransformation'), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 2], PURL('format'), "text/html", $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 2], PURL('language'), renarration_transformation_language[j], $rdf.sym(locationurl)));
        } else if (renarration_transformation_insert_value[j] === "after") {
          additions.push($rdf.st(file, RN('hasRenarrationTransformation'), blanknodes[i], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], OA('hasSelector'), blanknodes[i + 1], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 1], RDF('type'), RN('RangeSelector'), $rdf.sym(locationurl)));


          additions.push($rdf.st(blanknodes[i + 1], OA('hasStartSelector'), blanknodes[i + 3], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 3], RDF('type'), OA('XpathSelector'), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 3], RDF('value'), renarration_transformation_xpath[j], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 1], OA('hasEndSelector'), blanknodes[i + 4], $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 4], RDF('type'), OA('XpathSelector'), $rdf.sym(locationurl)));

          if (renarration_transformation_insert_next_xpath[j] !== null) {
            additions.push($rdf.st(blanknodes[i + 4], RDF('value'), renarration_transformation_insert_next_xpath[j], $rdf.sym(locationurl)));
          }

          additions.push($rdf.st(blanknodes[i], RN('createdAt'), $rdf.lit(renarration_transformation_date[j], '', XSD('dateTime')), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RN('hasAction'), $rdf.lit(renarration_transformation_action[j], '', XSD('string')), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RN('hasNarration'), blanknodes[i + 2], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 2], RDF('value'), renarration_transformation_new_content[j], $rdf.sym(locationurl)));

          additions.push($rdf.st(blanknodes[i + 2], RDF('type'), RN(renarration_transformation_media_type[j]), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i], RDF('type'), RN('RenarrationTransformation'), $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 2], PURL('format'), "text/html", $rdf.sym(locationurl)));
          additions.push($rdf.st(blanknodes[i + 2], PURL('language'), renarration_transformation_language[j], $rdf.sym(locationurl)));
        }
      } else if (renarration_transformation_action[j] === "remove") {
        additions.push($rdf.st(file, RN('hasRenarrationTransformation'), blanknodes[i], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], OA('hasSelector'), blanknodes[i + 1], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], RDF('type'), RN('RenarrationTransformation'), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 1], RDF('value'), renarration_transformation_xpath[j], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 1], RDF('type'), OA('XPathSelector'), $rdf.sym(locationurl)));

        additions.push($rdf.st(blanknodes[i], RN('createdAt'), $rdf.lit(renarration_transformation_date[j], '', XSD('dateTime')), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], RN('hasAction'), $rdf.lit(renarration_transformation_action[j], '', XSD('string')), $rdf.sym(locationurl)));
      } else if (renarration_transformation_action[j] === "replace") {
        additions.push($rdf.st(file, RN('hasRenarrationTransformation'), blanknodes[i], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], OA('hasSelector'), blanknodes[i + 1], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 1], RDF('value'), renarration_transformation_xpath[j], $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 1], RDF('type'), OA('XPathSelector'), $rdf.sym(locationurl)));

        additions.push($rdf.st(blanknodes[i], RN('createdAt'), $rdf.lit(renarration_transformation_date[j], '', XSD('dateTime')), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], RN('hasAction'), $rdf.lit(renarration_transformation_action[j], '', XSD('string')), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], RN('hasNarration'), blanknodes[i + 2], $rdf.sym(locationurl)));

        additions.push($rdf.st(blanknodes[i + 2], RDF('value'), renarration_transformation_new_content[j], $rdf.sym(locationurl)));

        additions.push($rdf.st(blanknodes[i + 2], RDF('type'), RN(renarration_transformation_media_type[j]), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i], RDF('type'), RN('RenarrationTransformation'), $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 2], PURL('format'), "text/html", $rdf.sym(locationurl)));
        additions.push($rdf.st(blanknodes[i + 2], PURL('language'), renarration_transformation_language[j], $rdf.sym(locationurl)));
      }

    }

    additions.push($rdf.st(file, RN('createdAt'), $rdf.lit(renarration_creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));
    additions.push($rdf.st($rdf.sym(renarration_source_url), RDF('type'), RN('Document'), $rdf.sym(locationurl)));

    console.log("additions", additions);

    let createAndUpdateTheFileObject = {
      location: locationurl,
      additions: additions,
      deletions: deletions
    };
    return createAndUpdateTheFileObject;
  },
  renarrationlist_creation_inside_renarrators_pod_turtle_operations: function(renarration_information_send_object) {




    let renarrator_name = renarration_information_send_object.name;
    let renarrator_webId = renarration_information_send_object.webId;
    let renarrator_role = renarration_information_send_object.role;
    let renarrator_email = renarration_information_send_object.email;
    let renarration_motivation = renarration_information_send_object.motivation;
    let renarration_source_url = renarration_information_send_object.source_url;
    let renarration_creation_time = renarration_information_send_object.creation_time;
    let renarration_unique_id = renarration_information_send_object.unique_id;

    let documentlocation = renarrator_webId + RenarrationSolidVariables.locationOfRenarrationFolderInTheSolidPodOfUser + "RNCNT-" + renarration_unique_id + ".ttl";
    let location = renarrator_webId + "/renarration/RN-CNT-ID-LIST.ttl";

    const additions = [];
    const deletions = [];
    const locationurl = location;
    let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
    let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
    let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
    let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
    let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");

    let blanknode = $rdf.blankNode();



    let file = $rdf.sym(documentlocation);
    additions.push($rdf.st(file, RDF('type'), RN('Renarration'), $rdf.sym(locationurl)));
    additions.push($rdf.st(file, RN('renarratedBy'), blanknode, $rdf.sym(locationurl)));
    additions.push($rdf.st(blanknode, RN('name'), renarrator_name, $rdf.sym(locationurl))); //change it problem
    additions.push($rdf.st(blanknode, RDF('type'), FOAF('Person'), $rdf.sym(locationurl)));
    //rn motivation
    additions.push($rdf.st(file, RN('hasMotivation'), RN(renarration_motivation), $rdf.sym(locationurl)));
    additions.push($rdf.st(file, RN('onSourceDocument'), $rdf.sym(renarration_source_url), $rdf.sym(locationurl)));


    additions.push($rdf.st(file, RN('renarratedAt'), $rdf.lit(renarration_creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));
    additions.push($rdf.st($rdf.sym(renarration_source_url), RDF('type'), RN('Document'), $rdf.sym(locationurl)));


    let createAndUpdateTheFileObject = {
      location: locationurl,
      additions: additions,
      deletions: deletions
    };

    return createAndUpdateTheFileObject;
  },
  renarrationlist_creation_inside_rnex_pod_turtle_operations: function(renarration_locatin_on_renarrators_pod, renarration_information_send_object) {

    let renarrator_name = renarration_information_send_object.name;
    let renarrator_role = renarration_information_send_object.role;
    let renarrator_email = renarration_information_send_object.email;
    let renarration_motivation = renarration_information_send_object.motivation;

    let renarrator_webId = renarration_information_send_object.webId;
    let renarration_source_url = renarration_information_send_object.source_url;
    let renarration_creation_time = renarration_information_send_object.creation_time;
    let renarration_unique_id = renarration_information_send_object.unique_id;

    let documentlocation = renarration_locatin_on_renarrators_pod + "#" + renarration_unique_id;
    let location = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;

    const additions = [];
    const deletions = [];
    const locationurl = location;
    let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
    let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
    let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
    let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
    let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
    let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
    let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
    let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

    const profile_card_me = $rdf.sym(renarrator_webId + RenarrationSolidVariables.textOfProfileCardMe);
    let blanknode = $rdf.blankNode();
    let file = $rdf.sym(documentlocation);

    additions.push($rdf.st(blanknode, RDF('type'), AS('Create'), $rdf.sym(locationurl)));
    additions.push($rdf.st(blanknode, AS('actor'), profile_card_me, $rdf.sym(locationurl)));
    additions.push($rdf.st(blanknode, AS('object'), file, $rdf.sym(locationurl)));
    additions.push($rdf.st(blanknode, AS('published'), $rdf.lit(renarration_creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));

    additions.push($rdf.st(file, RDF('type'), RN('Renarration'), $rdf.sym(locationurl)));
    additions.push($rdf.st(file, RN('onSourceDocument'), $rdf.sym(renarration_source_url), $rdf.sym(locationurl)));
    additions.push($rdf.st($rdf.sym(renarration_source_url), RDF('type'), RN('Document'), $rdf.sym(locationurl)));

    let createAndUpdateTheFileObject = {
      location: locationurl,
      additions: additions,
      deletions: deletions
    };

    return createAndUpdateTheFileObject;
  },
  getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromAPod: async function(number_of_renarrations_to_bring_in_each_page, table_footer_action_obj, locationurl) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    const renarrationReadPromise = new Promise((resolve, reject) => {
      fetcher.load(locationurl).then(res => {

        let renarration_documents_array = [];
        let sliced_renarration_documents_array = [];
        let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");

        store.match(null, LDP('contains'), null, null).map(st => {
          renarration_documents_array.push(st.object.value);
        });
        let reverse_renarration_documents_array = renarration_documents_array.slice();
        reverse_renarration_documents_array.reverse();

        let desired_array_for_the_page = SolidPodOperations.getTheRenarrationTableDesiredArrayForAGivenRenarrationArrayCommonFunction(reverse_renarration_documents_array, table_footer_action_obj, number_of_renarrations_to_bring_in_each_page);


        console.log("desired_array_for_the_page", desired_array_for_the_page);

        resolve({
          renarration_list_length: renarration_documents_array.length,
          desired_array_for_the_page: desired_array_for_the_page
        });

      }).catch(e => reject("error ", e));

    });

    return await renarrationReadPromise;
  },
  getTheRenarrationTableDesiredArrayForAGivenRenarrationArrayCommonFunction: function(renarration_documents_array, table_footer_action_obj, number_of_renarrations_to_bring_in_each_page) {
    let total_number_of_page_in_the_table = Math.floor(renarration_documents_array.length / number_of_renarrations_to_bring_in_each_page) + 1;
    let desired_array_for_the_page = [];
    if (table_footer_action_obj !== undefined && table_footer_action_obj !== null && table_footer_action_obj.current_page >= 1 && table_footer_action_obj.current_page <= total_number_of_page_in_the_table) {
      let current_page = table_footer_action_obj.current_page;
      let start_the_no_item;
      let end_the_no_item;

      if (table_footer_action_obj.action === "next") {
        start_the_no_item = number_of_renarrations_to_bring_in_each_page * (current_page - 1);
        end_the_no_item = start_the_no_item + number_of_renarrations_to_bring_in_each_page;

        renarration_documents_array.forEach((item, i) => {
          if (i >= start_the_no_item && i < end_the_no_item) {
            desired_array_for_the_page.push(item);
          }
        });
      } else if (table_footer_action_obj.action === "previous") {
        start_the_no_item = number_of_renarrations_to_bring_in_each_page * current_page;
        end_the_no_item = start_the_no_item - number_of_renarrations_to_bring_in_each_page;

        renarration_documents_array.forEach((item, i) => {
          if (i < start_the_no_item && i >= end_the_no_item) {
            desired_array_for_the_page.push(item);
          }
        });
      } else {
        start_the_no_item = 0;
        end_the_no_item = number_of_renarrations_to_bring_in_each_page;
        renarration_documents_array.forEach((item, i) => {
          if (i >= start_the_no_item && i < end_the_no_item) {
            desired_array_for_the_page.push(item);
          }
        });

      }

    }

    return desired_array_for_the_page;
  },
  getTheRenarrationsFromARenarratorsPodCommonFunction: async function(renarration_array_url_list) {


    const singleUrlRenarrationRead = (singleRenarrationUrl) => {
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);
      return new Promise((resolve, reject) => {

        fetcher.load(singleRenarrationUrl).then(res => {

          let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
          let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
          let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
          let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
          let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
          let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
          let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
          let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
          let OA = $rdf.Namespace("http://www.w3.org/ns/oa#");

          let renarration_document = {
            renarrationTransformation: []
          };
          let renarration_motivation = [];
          let test0 = store.match(null, RDF('type'), RN('Renarration'), null).map(st => {

            renarration_document.targetFileSourceUrlandUniqueId = st.subject.value;

            store.match($rdf.sym(st.subject.value), RN('hasMotivation'), null, null).map(st2 => {
              renarration_motivation.push(st2.object.value);
            });

            renarration_document.motivation = renarration_motivation;

            store.match($rdf.sym(st.subject.value), RN('onSourceDocument'), null, null).map(st3 => {
              renarration_document.onSourceDocument = st3.object.value;
            });

            store.match($rdf.sym(st.subject.value), RN('createdAt'), null, null).map(st4 => {
              renarration_document.renarratedAt = st4.object.value;
            });

            store.match($rdf.sym(st.subject.value), RN('renarratedBy'), null, null).map(st5 => {
              renarration_document.renarratedBy = st5.object.value;
            });

            store.match($rdf.sym(st.subject.value), RN('hasRenarrationTransformation'), null, null).map(st7 => {

              let renarration_transformation = {
                RangeSelector: {}
              };


              store.match($rdf.blankNode(st7.object.value), RN('hasAction'), null, null).map(st8 => {
                //  console.log("st8",st8.object.value);

                renarration_transformation.actionOnDocument = st8.object.value;

                renarration_transformation.hasAction = st8.object.value;
              });

              store.match($rdf.blankNode(st7.object.value), RN('createdAt'), null, null).map(st8 => {
                //console.log("st8",st8.object.value);
                renarration_transformation.createdAt = st8.object.value;
              });

              store.match($rdf.blankNode(st7.object.value), RN('hasNarration'), null, null).map(st8 => {

                store.match($rdf.blankNode(st8.object.value), RDF('type'), null, null).map(st9 => {
                  rdfType = st9.object.value;
                  if (rdfType === RN('Text').value || rdfType === RN('text').value) {
                    renarration_transformation.mediaType = "Text";
                  } else if (rdfType === RN('Image').value || rdfType === RN('image').value) {
                    renarration_transformation.mediaType = "Image";
                  } else if (rdfType === RN('Video').value || rdfType === RN('video').value) {
                    renarration_transformation.mediaType = "Video";
                  } else if (rdfType === RN('Audio').value || rdfType === RN('audio').value) {
                    renarration_transformation.mediaType = "Audio";
                  } else {
                    renarration_transformation.mediaType = "";
                  }
                });

                store.match($rdf.blankNode(st8.object.value), PURL('language'), null, null).map(st9 => {
                  renarration_transformation.language = st9.object.value;
                });
                store.match($rdf.blankNode(st8.object.value), RDF('value'), null, null).map(st9 => {
                  renarration_transformation.content = st9.object.value;
                });
              });

              store.match($rdf.blankNode(st7.object.value), OA('hasSelector'), null, null).map(st8 => {
                //console.log("st8",st8.object.value);

                if (renarration_transformation.hasAction === "insert") {
                  store.match($rdf.blankNode(st8.object.value), OA('hasStartSelector'), null, null).map(st9 => {
                    store.match($rdf.blankNode(st9.object.value), RDF('value'), null, null).map(st10 => {
                      renarration_transformation.RangeSelector.startSelectorXpathValue = st10.object.value;
                    });
                  });
                  store.match($rdf.blankNode(st8.object.value), OA('hasEndSelector'), null, null).map(st9 => {
                    store.match($rdf.blankNode(st9.object.value), RDF('value'), null, null).map(st10 => {
                      renarration_transformation.RangeSelector.endSelectorXpathValue = st10.object.value;
                    });
                  });
                } else if (renarration_transformation.hasAction === "replace") {

                  store.match($rdf.blankNode(st8.object.value), RDF('value'), null, null).map(st9 => {
                    renarration_transformation.xpathValue = st9.object.value;
                  });

                } else if (renarration_transformation.hasAction === "remove") {
                  store.match($rdf.blankNode(st8.object.value), RDF('value'), null, null).map(st9 => {
                    renarration_transformation.xpathValue = st9.object.value;
                  });
                }

              });

              renarration_document.renarrationTransformation.push(renarration_transformation);

            });



          });

          resolve(renarration_document);

        }).catch(err => {
          console.log("failed fetcher load", err);
          reject(err);
        });

      });
    };

    let allTheSingleUrlRenarrationPromises = [];
    renarration_array_url_list.forEach(singleUrl => {

      allTheSingleUrlRenarrationPromises.push(singleUrlRenarrationRead(singleUrl));
    });

    const renarrationFulfilledReadPromise = new Promise((resolve, reject) => {
      Promise.allSettled(allTheSingleUrlRenarrationPromises).then(res => {
        let fulfilledRenarrations = [];

        res.forEach(aRenarration => {
          if (aRenarration.status === "fulfilled") {
            fulfilledRenarrations.push(aRenarration.value);
          } else {
            console.log(aRenarration, "problem with the renarration");
          }
        });

        resolve(fulfilledRenarrations);
      }).catch(err => {
        reject(err);
      });
    });

    return await renarrationFulfilledReadPromise;

  },
  getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod: async function(number_of_renarrations_to_bring_in_each_page, table_footer_action_obj, url_of_the_renarrationFiles_folder) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    let renarration_array_url_list = await SolidPodOperations.getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromAPod(number_of_renarrations_to_bring_in_each_page, table_footer_action_obj, url_of_the_renarrationFiles_folder).then(s => {

      return s;
    }).catch(e => console.log("error read contains renarration files", e));

    let desired_renarrations = await SolidPodOperations.getTheRenarrationsFromARenarratorsPodCommonFunction(renarration_array_url_list.desired_array_for_the_page);

    //  let total_number_of_page_in_the_table = Math.floor(renarration_array_url_list.renarration_list_length / number_of_renarrations_to_bring_in_each_page) + 1;
    let total_number_of_page_in_the_table = Math.floor(((renarration_array_url_list.renarration_list_length - 1) >= 0 ? (renarration_array_url_list.renarration_list_length - 1) : 0) / number_of_renarrations_to_bring_in_each_page) + 1;
    return {
      total_number_of_page_in_the_table: total_number_of_page_in_the_table,
      number_of_renarrations_to_bring_in_each_page: number_of_renarrations_to_bring_in_each_page,
      renarration_list_length: renarration_array_url_list.renarration_list_length,
      desired_renarrations: desired_renarrations
    };
  },
  getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPodWithInformationFromRnExCentralPod: async function(number_of_renarrations_to_bring_in_each_page, url_of_the_rnex_renarration_list, table_footer_action_obj, desiredOnSourceDocument, desiredRenarratedBy) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    let renarration_array_url_list = await SolidPodOperations.getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPodCommonFunction(number_of_renarrations_to_bring_in_each_page, url_of_the_rnex_renarration_list, table_footer_action_obj, desiredOnSourceDocument, desiredRenarratedBy).then(s => {

      return s;
    }).catch(e => console.log("error read contains renarration files", e));

    let desired_renarrations = await SolidPodOperations.getTheRenarrationsFromARenarratorsPodCommonFunction(renarration_array_url_list.desired_array_for_the_page);

    let total_number_of_page_in_the_table = Math.floor(((renarration_array_url_list.renarration_list_length - 1) >= 0 ? (renarration_array_url_list.renarration_list_length - 1) : 0) / number_of_renarrations_to_bring_in_each_page) + 1;

    return {
      total_number_of_page_in_the_table: total_number_of_page_in_the_table,
      number_of_renarrations_to_bring_in_each_page: number_of_renarrations_to_bring_in_each_page,
      renarration_list_length: renarration_array_url_list.renarration_list_length,
      desired_renarrations: desired_renarrations
    };
  },
  getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod: async function(number_of_last_renarrations, desiredOnSourceDocument) {

    let url_of_rnex_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;

    let renarration_array_url_list = await SolidPodOperations.getTheRenarrationURLSOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_pod_list_file, desiredOnSourceDocument).then(s => {

      return s.slice();
    }).catch(e => console.log("error read contains renarration files", e));

    return await SolidPodOperations.getTheRenarrationsFromARenarratorsPodCommonFunction(renarration_array_url_list);

  },
  getTheRenarrationURLSOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPod: async function(number_of_last_renarrations, url_of_rnex_pod_list_file, desiredOnSourceDocument) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    const renarrationReadPromise = new Promise((resolve, reject) => {


      fetcher.load(url_of_rnex_pod_list_file).then(res => {

        let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
        let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
        let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
        let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
        let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
        let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
        let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
        let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

        let activityStreamPartOf_renarration_document_list = [];
        let desired_array_of_renarrations = [];

        let activityStreamPartOfRenarration = store.match(null, RDF('type'), AS('Create'), null).map(st => {
          let activityStreamPartOf_renarration_document = {};

          store.match($rdf.blankNode(st.subject.value), AS('published'), null, null).map(st4 => {
            activityStreamPartOf_renarration_document.renarratedAt = st4.object.value;
          });

          store.match($rdf.blankNode(st.subject.value), AS('actor'), null, null).map(st5 => {
            activityStreamPartOf_renarration_document.renarratedBy = st5.object.value;
          });
          store.match($rdf.blankNode(st.subject.value), AS('object'), null, null).map(st5 => {
            activityStreamPartOf_renarration_document.onTargetDocument = st5.object.value;
          });
          activityStreamPartOf_renarration_document_list.push(activityStreamPartOf_renarration_document);

        });

        activityStreamPartOf_renarration_document_list.forEach(renarrationActivityPart => {
          let onSourceDocumentPartOfRenarration = store.match($rdf.sym(renarrationActivityPart.onTargetDocument), RN('onSourceDocument'), null, null).map(st => {
            renarrationActivityPart.onSourceDocument = st.object.value;
          });
        });

        for (let i = 0; i < activityStreamPartOf_renarration_document_list.length; i++) {
          if (activityStreamPartOf_renarration_document_list[i].onSourceDocument === desiredOnSourceDocument) {
            desired_array_of_renarrations.push(activityStreamPartOf_renarration_document_list[i]);
          }
        }

        if (RenarrationSolidVariables.getAllTheRenarrationsForTheNotification === desiredOnSourceDocument) {
          desired_array_of_renarrations = activityStreamPartOf_renarration_document_list.slice();
        }

        let desired_number_of_renarrations = desired_array_of_renarrations.sort((a, b) => new Date(b.renarratedAt) - new Date(a.renarratedAt)).slice(0, number_of_last_renarrations);

        let renarrationsOnTargetDocumentList = [];
        desired_number_of_renarrations.forEach(renarrationitem => renarrationsOnTargetDocumentList.push(renarrationitem.onTargetDocument));

        if (RenarrationSolidVariables.getAllTheRenarrationsForTheNotification === desiredOnSourceDocument) {
          renarrationsOnTargetDocumentList = desired_number_of_renarrations.slice();
        }
        resolve(renarrationsOnTargetDocumentList);

      });

    }).catch(err => {
      console.log("failed fetcher load", err);
      reject(err);
    });



    return await renarrationReadPromise;
  },
  getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPodCommonFunction: async function(number_of_renarrations_to_bring_in_each_page, url_of_rnex_pod_list_file, table_footer_action_obj, desiredOnSourceDocument = undefined, desiredRenarratedBy = undefined) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    const renarrationReadPromise = new Promise((resolve, reject) => {


      fetcher.load(url_of_rnex_pod_list_file).then(res => {

        let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
        let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
        let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
        let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
        let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
        let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
        let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
        let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

        let activityStreamPartOf_renarration_document_list = [];
        let desired_array_of_renarrations = [];

        let activityStreamPartOfRenarration = store.match(null, RDF('type'), AS('Create'), null).map(st => {
          let activityStreamPartOf_renarration_document = {};

          store.match($rdf.blankNode(st.subject.value), AS('published'), null, null).map(st4 => {
            activityStreamPartOf_renarration_document.renarratedAt = st4.object.value;
          });

          store.match($rdf.blankNode(st.subject.value), AS('actor'), null, null).map(st5 => {
            activityStreamPartOf_renarration_document.renarratedBy = st5.object.value;
          });
          store.match($rdf.blankNode(st.subject.value), AS('object'), null, null).map(st5 => {
            activityStreamPartOf_renarration_document.onTargetDocument = st5.object.value;
          });
          activityStreamPartOf_renarration_document_list.push(activityStreamPartOf_renarration_document);

        });

        activityStreamPartOf_renarration_document_list.forEach(renarrationActivityPart => {
          store.match($rdf.sym(renarrationActivityPart.onTargetDocument), RN('onSourceDocument'), null, null).map(st => {
            renarrationActivityPart.onSourceDocument = st.object.value;
          });
        });

        if (desiredOnSourceDocument) {
          for (let i = 0; i < activityStreamPartOf_renarration_document_list.length; i++) {
            if (activityStreamPartOf_renarration_document_list[i].onSourceDocument === desiredOnSourceDocument) {
              desired_array_of_renarrations.push(activityStreamPartOf_renarration_document_list[i]);
            }
          }
        } else if (desiredRenarratedBy) {
          for (let i = 0; i < activityStreamPartOf_renarration_document_list.length; i++) {
            if (activityStreamPartOf_renarration_document_list[i].renarratedBy === desiredRenarratedBy) {
              desired_array_of_renarrations.push(activityStreamPartOf_renarration_document_list[i]);
            }
          }
          //desired_array_of_renarrations = activityStreamPartOf_renarration_document_list.slice();
        }

        let desired_number_of_renarrations = desired_array_of_renarrations.sort((a, b) => new Date(b.renarratedAt) - new Date(a.renarratedAt)).slice();

        let renarrationsOnTargetDocumentList = [];
        desired_number_of_renarrations.forEach(renarrationitem => renarrationsOnTargetDocumentList.push(renarrationitem.onTargetDocument));

        let desired_array_for_the_page = SolidPodOperations.getTheRenarrationTableDesiredArrayForAGivenRenarrationArrayCommonFunction(renarrationsOnTargetDocumentList, table_footer_action_obj, number_of_renarrations_to_bring_in_each_page);

        resolve({
          renarration_list_length: renarrationsOnTargetDocumentList.length,
          desired_array_for_the_page: desired_array_for_the_page
        });


      });

    }).catch(err => {
      console.log("failed fetcher load", err);
      reject(err);
    });



    return await renarrationReadPromise;
  },
  getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromOtherRenarratorsPod: async function(number_of_last_renarrations, locationurl) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    const renarrationReadPromise = new Promise((resolve, reject) => {
      fetcher.load(locationurl).then(res => {

        let renarration_documents_array = [];
        let sliced_renarration_documents_array = [];
        let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");

        store.match(null, LDP('contains'), null, null).map(st => {
          renarration_documents_array.push(st.object.value);
        });
        if (number_of_last_renarrations > 0) {
          sliced_renarration_documents_array = renarration_documents_array.slice(renarration_documents_array.length - number_of_last_renarrations, renarration_documents_array.length);
        } else {
          sliced_renarration_documents_array = renarration_documents_array.slice();
        }
        resolve(sliced_renarration_documents_array);
      }).catch(e => reject("error ", e));

    });

    return await renarrationReadPromise;
  },
  getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromTheOtherRenarratorsPod: async function(number_of_last_renarrations, url_of_the_renarrationFiles_folder) {
    const store = $rdf.graph();
    const fetcher = new $rdf.Fetcher(store);

    let renarration_array_url_list = await SolidPodOperations.getTheUrlOfChosenNumberOfRenarrationTurtleFilesFromOtherRenarratorsPod(number_of_last_renarrations, url_of_the_renarrationFiles_folder).then(s => {
      return s.slice();
    }).catch(e => console.log("error read contains renarration files", e));

    console.log("renarration_array_url_list", renarration_array_url_list);

    const renarrationReadPromise = new Promise((resolve, reject) => {

      fetcher.load(renarration_array_url_list).then(res => {

        let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
        let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
        let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
        let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
        let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
        let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
        let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

        let renarration_document_list = [];

        let test0 = store.match(null, RDF('type'), RN('Renarration'), null).map(st => {
          let renarration_document = {
            renarrationTransformation: []
          };
          renarration_document.targetFileSourceUrlandUniqueId = st.subject.value;
          store.match($rdf.sym(st.subject.value), RN('hasMotivation'), null, null).map(st2 => {
            renarration_document.motivation = st2.object.value;
          });
          store.match($rdf.sym(st.subject.value), RN('onSourceDocument'), null, null).map(st3 => {
            renarration_document.onSourceDocument = st3.object.value;
          });

          store.match($rdf.sym(st.subject.value), RN('renarratedAt'), null, null).map(st4 => {
            renarration_document.renarratedAt = st4.object.value;
          });

          store.match($rdf.sym(st.subject.value), RN('renarratedBy'), null, null).map(st5 => {
            store.match($rdf.blankNode(st5.object.value), RN('name'), null, null).map(st8 => {
              renarration_document.renarratedBy = st8.object.value;
            });
          });

          store.match($rdf.sym(st.subject.value), RN('hasRenarrationTransformation'), null, null).map(st7 => {

            let renarration_transformation = {};


            store.match($rdf.blankNode(st7.object.value), RN('actionOnDocument'), null, null).map(st8 => {
              //  console.log("st8",st8.object.value);
              renarration_transformation.actionOnDocument = st8.object.value;
            });
            store.match($rdf.blankNode(st7.object.value), PURL('language'), null, null).map(st8 => {
              //  console.log("st8",st8.object.value);
              renarration_transformation.language = st8.object.value;
            });
            store.match($rdf.blankNode(st7.object.value), RN('createdAt'), null, null).map(st8 => {
              //console.log("st8",st8.object.value);
              renarration_transformation.createdAt = st8.object.value;
            });
            store.match($rdf.blankNode(st7.object.value), RN('hasNarration'), null, null).map(st8 => {
              //console.log("st8",st8.object.value);
              store.match($rdf.blankNode(st8.object.value), CNT('content'), null, null).map(st9 => {
                //console.log("st8",st8.object.value);
                renarration_transformation.content = st9.object.value;
              });
            });
            store.match($rdf.blankNode(st7.object.value), RN('sourceSelection'), null, null).map(st8 => {
              //console.log("st8",st8.object.value);
              store.match($rdf.blankNode(st8.object.value), RDF('value'), null, null).map(st9 => {
                //console.log("st8",st8.object.value);
                renarration_transformation.xpathValue = st9.object.value;
              });
            });

            renarration_document.renarrationTransformation.push(renarration_transformation);

          });

          renarration_document_list.push(renarration_document);

        });

        resolve(renarration_document_list);

      }).catch(err => {
        console.log("failed fetcher load", err);
        reject(err);
      });

    });

    return await renarrationReadPromise;
  },
  getTheNotificationsFromTheRnexPod: async function(lastViewTimeOfNotificationsDate, peopleWhoAreFollowed) {
    let lastViewTimeOfNotifications = lastViewTimeOfNotificationsDate;
    if (!lastViewTimeOfNotificationsDate) {
      lastViewTimeOfNotifications = null;
    }

    let url_of_rnex_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfResponseListInTheSolidPodOfRnEx;
    let desiredOnSourceDocument = RenarrationSolidVariables.getAllTheResponsesForTheNotification;
    let desiredOnSourceDocumentAllTheRenarrations = RenarrationSolidVariables.getAllTheRenarrationsForTheNotification;
    let number_of_last_renarrations;
    let url_of_rnex_renarrations_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;

    let listOfPeopleWhoAreFollowed = [];
    peopleWhoAreFollowed.forEach((item, i) => {
      listOfPeopleWhoAreFollowed.push(item.object);
    });

    let response_array_url_list = await SolidPodOperations.response_turtle_operations.getTheResponseURLSOfChosenNumberOfResponseTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_pod_list_file, desiredOnSourceDocument).then(responseArray => {
      console.log(" responseArray", responseArray);

      let filteredResponseArray = [];
      responseArray.forEach((responseItem, i) => {
        listOfPeopleWhoAreFollowed.forEach((targetPerson, i) => {
          if (responseItem.isCreatedBy === targetPerson) {
            filteredResponseArray.push(responseItem);
          }
        });

      });
      let timefilteredResponseArray = filteredResponseArray.filter(item => new Date(item.published) > new Date(lastViewTimeOfNotifications));
      console.log("timefilteredResponseArray", timefilteredResponseArray);

      return timefilteredResponseArray.slice();
    }).catch(e => console.log("error read contains renarration files", e));

    let renarration_array_url_list = await SolidPodOperations.getTheRenarrationURLSOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_renarrations_pod_list_file, desiredOnSourceDocumentAllTheRenarrations).then(renarrationArray => {
      console.log("getTheRenarrationURLSOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPod", renarrationArray);


      let filteredRenarrationArray = [];
      renarrationArray.forEach((renarrationItem, i) => {
        listOfPeopleWhoAreFollowed.forEach((targetPerson, i) => {
          if (renarrationItem.renarratedBy === targetPerson) {
            filteredRenarrationArray.push(renarrationItem);
          }
        });

      });

      let timefilteredRenarrationArray = filteredRenarrationArray.filter(item => new Date(item.renarratedAt) > new Date(lastViewTimeOfNotifications));
      console.log("timefilteredRenarrationArray", timefilteredRenarrationArray);

      return timefilteredRenarrationArray.slice();
    }).catch(e => console.log("error read contains renarration files", e));

    let notificationResponseAndRenarrationList = {
      renarrationList: renarration_array_url_list,
      responseList: response_array_url_list
    };

    return notificationResponseAndRenarrationList;

  },
  getTheWebIdFromTheUrlWithRegex: function(renarrator_profile_card) {
    let regex = /(http[s]?:\/\/)?([^\/\s]+)/g;
    let val = renarrator_profile_card.match(regex);
    let ret_arr = [];
    ret_arr.push(val[0]);
    ret_arr.push(val[0] + "/profile/card#me");

    return ret_arr;
  },
  recommending_turtle_operations: {
    getAllTheResponseItemsFromTheRnExPod: async function() {
      let url_of_rnex_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfResponseListInTheSolidPodOfRnEx;
      let desiredOnSourceDocument = RenarrationSolidVariables.getAllTheResponsesForTheNotification;
      let number_of_last_renarrations;

      let response_array_url_list = await SolidPodOperations.response_turtle_operations.getTheResponseURLSOfChosenNumberOfResponseTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_pod_list_file, desiredOnSourceDocument).then(responseArray => {
        console.log(" responseArray", responseArray);

        return responseArray;
      }).catch(e => console.log("error read contains renarration files", e));

      return response_array_url_list;
    },
    getAllTheRenarrationItemsFromTheRnExPod: async function() {
      let number_of_last_renarrations;
      let desiredOnSourceDocumentAllTheRenarrations = RenarrationSolidVariables.getAllTheRenarrationsForTheNotification;
      let url_of_rnex_renarrations_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;

      let renarration_array_url_list = await SolidPodOperations.getTheRenarrationURLSOfChosenNumberOfRenarrationTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_renarrations_pod_list_file, desiredOnSourceDocumentAllTheRenarrations).then(renarrationArray => {
        console.log("renarrationArray", renarrationArray);

        return renarrationArray;
      }).catch(e => console.log("error read contains renarration files", e));

      return renarration_array_url_list;
    },
    giveTheListOfRenarratorsByGettingResponseItems: async function(currentRenarrator) {
      let responseItems = await SolidPodOperations.recommending_turtle_operations.getAllTheResponseItemsFromTheRnExPod();

      let renarrationItems = await SolidPodOperations.recommending_turtle_operations.getAllTheRenarrationItemsFromTheRnExPod();

      await console.log("responseItems", responseItems, "renarrationItems", renarrationItems);


      function responseItemObjectArrayConversionToRenarratorToRenarratorObjectArray(responseItemObjectArray) {
        let renarratorToRenarratorObjectArray = {};
        responseItemObjectArray.forEach(item => {
          let responOnRenarrationRenarrator = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(item.responseOnRenarration)[1];

          if (!renarratorToRenarratorObjectArray[item.isCreatedBy]) {
            renarratorToRenarratorObjectArray[item.isCreatedBy] = {};
            renarratorToRenarratorObjectArray[item.isCreatedBy][responOnRenarrationRenarrator] = true;
          } else {
            if (!renarratorToRenarratorObjectArray[item.isCreatedBy][responOnRenarrationRenarrator]) {
              renarratorToRenarratorObjectArray[item.isCreatedBy][responOnRenarrationRenarrator] = true;
            }
          }
        });
        return renarratorToRenarratorObjectArray;
      }

      function renarrationItemsObjectArrayConversionToRenarratorRenarrationKeyValueObjectArray(renarratorItemObjectArray) {
        let renarratorToRenarrationObjectArray = {};
        renarratorItemObjectArray.forEach(item => {
          let renarrationItem = {
            onSourceDocument: item.onSourceDocument,
            onTargetDocument: item.onTargetDocument,
            renarratedAt: item.renarratedAt,
            renarratedBy: item.renarratedBy
          };
          if (!renarratorToRenarrationObjectArray[item.renarratedBy]) {
            renarratorToRenarrationObjectArray[item.renarratedBy] = [];
            renarratorToRenarrationObjectArray[item.renarratedBy].push(renarrationItem);
          } else {
            renarratorToRenarrationObjectArray[item.renarratedBy].push(renarrationItem);
          }
        });
        return renarratorToRenarrationObjectArray;
      }

      function renarrationArray(renarratorsArrayList, renarrationItems) {
        let secondDeriveArray = [];
        renarratorsArrayList.forEach((renarrator) => {
          if (renarrationItems[renarrator]) {
            secondDeriveArray.push(renarrationItems[renarrator]);
          }
        });
        return secondDeriveArray;
      }

      function getTheSecondDegreeRenarrators(currentRenarrator, renarratorsListObject) {
        let firstDegreeRenarratorsAll = Object.keys(renarratorsListObject[currentRenarrator]);
        let firstDegreeRenarrators = firstDegreeRenarratorsAll.filter(function(value, index, arr) {
          return value !== currentRenarrator;
        });
        let secondDegreeRenarrators = [];
        firstDegreeRenarrators.forEach(item => {
          if (renarratorsListObject[item]) {
            if (!!Object.keys(renarratorsListObject[item]).length) {
              let secDegreeRenarrators = Object.keys(renarratorsListObject[item]);
              secondDegreeRenarrators.push(secDegreeRenarrators);
            }
          }
        });

        let allRens = [];
        secondDegreeRenarrators.forEach(item => {
          allRens = allRens.concat(item);
        });
        let uq = [...new Set(allRens)];
        let allResults = uq.filter(function(value, index, arr) {
          return value !== currentRenarrator;
        });
        let toRemoveFirstDegreeAndCurrentRenarrators = firstDegreeRenarrators.concat(currentRenarrator);
        let allResultsWithoutFirstDegree = allResults.filter(function(el) {
          return !toRemoveFirstDegreeAndCurrentRenarrators.includes(el);
        });
        let firstAndSecondDegreeCombinedResultsNotUnique = firstDegreeRenarrators.concat(allResults);
        let firstAndSecondDegreeCombinedResultsUnique = [...new Set(firstAndSecondDegreeCombinedResultsNotUnique)];

        return {
          firstDegree: firstDegreeRenarrators,
          secondDegree: allResultsWithoutFirstDegree,
          secondDegreeWithoutRemovalOfFirstDegree: allResults,
          firstAndSecondDegreeCombined: firstAndSecondDegreeCombinedResultsUnique
        };
      }

      function renarrationArrayFinalRenarrationsToOnTargetDocumentArray(renarrationArrayFinal) {
        let renarrationArrayFinalObject = [];
        renarrationArrayFinal.forEach(item => {
          renarrationArrayFinalObject = renarrationArrayFinalObject.concat(item);
        });
        let renarrationArrayFinalObjectTargetDocument = [];
        renarrationArrayFinalObject.forEach(item => {
          renarrationArrayFinalObjectTargetDocument.push(item.onTargetDocument);
        });
        return {
          renarrationArrayFinalObject: renarrationArrayFinalObject,
          renarrationArrayFinalObjectTargetDocument: renarrationArrayFinalObjectTargetDocument
        };
      }

      let renarrationItemsWithRenarratorKey = renarrationItemsObjectArrayConversionToRenarratorRenarrationKeyValueObjectArray(renarrationItems);
      let renarratorObjectArrayItemsList = responseItemObjectArrayConversionToRenarratorToRenarratorObjectArray(responseItems);
      let renarratorArrayItemsList = getTheSecondDegreeRenarrators(currentRenarrator, renarratorObjectArrayItemsList);
      let renarrationArrayFinalRenarrations = renarrationArray(renarratorArrayItemsList.secondDegree, renarrationItemsWithRenarratorKey);
      let renarrationArrayFinalRenarrationsWithAndWithoutOnTarget = renarrationArrayFinalRenarrationsToOnTargetDocumentArray(renarrationArrayFinalRenarrations);
      console.log("renarrationItemsObjectArrayConversionToRenarratorRenarrationKeyValueObjectArray", renarrationItemsWithRenarratorKey);

      console.log("renarratorObjectArrayItemsList", renarratorObjectArrayItemsList);

      console.log("getTheSecondDegreeRenarrators", renarratorArrayItemsList);

      console.log("renarrationArray", renarrationArrayFinalRenarrations);

      return renarrationArrayFinalRenarrationsWithAndWithoutOnTarget;
    },
    getTheSearchedRenarrationsFromARenarratorsPod: async function(operation_type, text_value) {
      let date_value = "date_value";
      let motivation_value = "motivation_value";
      let language_value = "language_value";
      let renarrator_value = "renarrator_value";
      let document_value = "document_value";
      let contains_value = "contains_value";
      let result;

      let renarrationItems = await SolidPodOperations.recommending_turtle_operations.getAllTheRenarrationItemsFromTheRnExPod();

      await console.log("renarrationItems", renarrationItems);

      if (operation_type === renarrator_value) {
        result = renarrationItems.filter(renarrationItem => renarrationItem.renarratedBy === text_value);
      } else if (operation_type === document_value) {
        result = renarrationItems.filter(renarrationItem => renarrationItem.onSourceDocument === text_value);
      } else if (operation_type === date_value) {
        result = renarrationItems.filter(renarrationItem => new Date(renarrationItem.renarratedAt) > new Date(text_value));
      }
      console.log("result", result);
      return result;



    },
    getTheSearchedRenarrationsFromARNExPodWithSPARQL: async function(operation_type, text_value) {
      let date_value = "date_value";
      let motivation_value = "motivation_value";
      let language_value = "language_value";
      let renarrator_value = "renarrator_value";
      let document_value = "document_value";
      let contains_value = "contains_value";
      let result;

      function sparqlQueryFunc(Renarrator = "?Renarrator", SourceDocument = "?SourceDocument", Renarration = "?Renarration", PublishDate = "?PublishDate") {

        const sparqlQuery = `
  PREFIX RDF: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX RNSOC : <${RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology}>
  PREFIX CNT : <http://www.w3.org/2011/content#>
  PREFIX PURL : <http://purl.org/dc/elements/1.1/>
  PREFIX FOAF : <http://xmlns.com/foaf/0.1/>
  PREFIX XSD : <http://www.w3.org/2001/XMLSchema#>
  PREFIX LDP : <http://www.w3.org/ns/ldp#>
  PREFIX OWL: <http://www.w3.org/2002/07/owl#>
  PREFIX OA : <http://www.w3.org/ns/oa#>
  PREFIX AS: <https://www.w3.org/ns/activitystreams#>

  SELECT ?Renarrator  ?PublishDate ?Renarration  ?SourceDocument WHERE {
  ?blankNode   a AS:Create;
  AS:object ${Renarration} ;
  AS:published ${PublishDate};
  AS:actor ${Renarrator} .

  ${Renarration}  <${RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology}onSourceDocument> ${SourceDocument}.
  }`;
        return sparqlQuery;
      }



      let sparqlToQuery;

      if (operation_type === renarrator_value) {
        let renarrator = "<" + text_value + ">";
        sparqlToQuery = sparqlQueryFunc(renarrator, "?SourceDocument", "?Renarration", "?PublishDate");
      } else if (operation_type === document_value) {
        let sourceDocument = "<" + text_value + ">";
        sparqlToQuery = sparqlQueryFunc("?Renarrator", sourceDocument, "?Renarration", "?PublishDate");
      } else if (operation_type === date_value) {

        sparqlToQuery = sparqlQueryFunc();

      }


      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      let location_of_rnex = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;

      const aPromise = new Promise((resolve, reject) => {


        fetcher.load(location_of_rnex).then(res => {

          const query = $rdf.SPARQLToQuery(sparqlToQuery, false, store);
          let renarrationArray = [];
          store.query(query, function(result) {
            let renarrationItem;
            if (operation_type === renarrator_value) {
              renarrationItem = {
                onTargetDocument: result['?Renarration'].value,
                renarratedBy: text_value,
                onSourceDocument: result['?SourceDocument'].value,
                renarratedAt: result['?PublishDate'].value
              };
              renarrationArray.push(renarrationItem);
            } else if (operation_type === document_value) {
              renarrationItem = {
                onTargetDocument: result['?Renarration'].value,
                renarratedBy: result['?Renarrator'].value,
                onSourceDocument: text_value,
                renarratedAt: result['?PublishDate'].value
              };
              renarrationArray.push(renarrationItem);
            } else if (operation_type === date_value) {
              renarrationItem = {
                onTargetDocument: result['?Renarration'].value,
                renarratedBy: result['?Renarrator'].value,
                onSourceDocument: result['?SourceDocument'].value,
                renarratedAt: result['?PublishDate'].value
              };
              if (renarrationItem && renarrationItem.renarratedAt && (new Date(renarrationItem.renarratedAt) > new Date(text_value))) {
                renarrationArray.push(renarrationItem);
              }
            }

          }, null, () => {
            console.log("renarrationArray", renarrationArray);
            resolve(renarrationArray);
          });
        });
      }).catch(err => {
        console.log("failed fetcher load", err);
      });

      return await aPromise;
    }
  },
  response_turtle_operations: {
    create_response_comment_file_on_the_renarrators_pod: function(renarratorsProfileCard, renarration_location, response_location, comment, comment_information) {



      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = renarratorsProfileCard.webIdWithoutProfileCard + response_location;
      const location_url_of_renarration_with_id = locationurl + "#" + comment_information.unique_id;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();
      let blanknodes = [];

      let file1 = $rdf.sym(renarration_location);
      let file = $rdf.sym(location_url_of_renarration_with_id);
      additions.push($rdf.st(file1, RDF('type'), OWL('NamedIndividual'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file1, RDF('type'), RNSOC('Renarration'), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RDF('type'), RNSOC('Comment'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file, RNSOC('hasCommentValue'), $rdf.lit(comment, '', XSD('string')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('isCreatedBy'), profile_card_me, $rdf.sym(locationurl)));

      additions.push($rdf.st(profile_card_me, RDF('type'), RNSOC('Renarrator'), $rdf.sym(locationurl)));
      additions.push($rdf.st(profile_card_me, RDF('type'), FOAF('Person'), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('respondedAt'), $rdf.lit(comment_information.creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));


      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;

    },
    create_response_comment_file_on_the_rnex_pod: function(renarratorsProfileCard, renarration_location, response_location, comment, comment_information) {

      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfResponseListInTheSolidPodOfRnEx;
      const location_url_of_renarration_with_id = renarratorsProfileCard.webIdWithoutProfileCard + response_location + "#" + comment_information.unique_id;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
      let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

      let file1 = $rdf.sym(renarration_location);
      let file = $rdf.sym(location_url_of_renarration_with_id);

      let blanknode3 = $rdf.blankNode();
      additions.push($rdf.st(blanknode3, RDF('type'), AS('Create'), $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('actor'), profile_card_me, $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('object'), file, $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('published'), $rdf.lit(comment_information.creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RDF('type'), RNSOC('Comment'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));
      additions.push($rdf.st($rdf.sym(file1), RDF('type'), RNSOC('Renarration'), $rdf.sym(locationurl)));


      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;


    },
    create_response_rate_file_on_the_renarrators_pod: function(renarratorsProfileCard, renarration_location, response_location, rate, rate_information) {



      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = renarratorsProfileCard.webIdWithoutProfileCard + response_location;
      const location_url_of_renarration_with_id = locationurl + "#" + rate_information.unique_id;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();
      let blanknodes = [];

      let file1 = $rdf.sym(renarration_location);
      let file = $rdf.sym(location_url_of_renarration_with_id);
      additions.push($rdf.st(file1, RDF('type'), OWL('NamedIndividual'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file1, RDF('type'), RNSOC('Renarration'), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RDF('type'), RNSOC('Rate'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file, RNSOC('hasRateValue'), $rdf.lit(rate, '', XSD('int')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('isCreatedBy'), profile_card_me, $rdf.sym(locationurl)));

      additions.push($rdf.st(profile_card_me, RDF('type'), RNSOC('Renarrator'), $rdf.sym(locationurl)));
      additions.push($rdf.st(profile_card_me, RDF('type'), FOAF('Person'), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('respondedAt'), $rdf.lit(rate_information.creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));


      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;

    },
    create_response_rate_file_on_the_rnex_pod: function(renarratorsProfileCard, renarration_location, response_location, rate, rate_information) {

      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfResponseListInTheSolidPodOfRnEx;
      const location_url_of_renarration_with_id = renarratorsProfileCard.webIdWithoutProfileCard + response_location + "#" + rate_information.unique_id;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
      let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();
      let blanknodes = [];

      let file1 = $rdf.sym(renarration_location);
      let file = $rdf.sym(location_url_of_renarration_with_id);


      let blanknode3 = $rdf.blankNode();
      additions.push($rdf.st(blanknode3, RDF('type'), AS('Create'), $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('actor'), profile_card_me, $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('object'), file, $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode3, AS('published'), $rdf.lit(rate_information.creation_time, '', XSD('dateTime')), $rdf.sym(locationurl)));

      additions.push($rdf.st(file, RDF('type'), RNSOC('Rate'), $rdf.sym(locationurl)));
      additions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));

      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;


    },
    read_response_file_from_the_renarrators_pod: function() {

    },
    read_response_file_from_the_rnex_pod: function() {

    },
    delete_response_file_from_the_renarrators_pod: function() {

      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = "";
      const location_url_of_renarration_with_id = locationurl + "#asd1256";
      const profile_card_me = $rdf.sym("");
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();
      let blanknodes = [];

      let file1 = $rdf.sym("");
      let file = $rdf.sym(location_url_of_renarration_with_id);
      deletions.push($rdf.st(file1, RDF('type'), OWL('NamedIndividual'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file1, RDF('type'), RNSOC('Renarration'), $rdf.sym(locationurl)));

      deletions.push($rdf.st(file, RDF('type'), RNSOC('Rate'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file, RNSOC('isCreatedBy'), profile_card_me, $rdf.sym(locationurl)));

      deletions.push($rdf.st(profile_card_me, RDF('type'), RNSOC('Renarrator'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(profile_card_me, RDF('type'), FOAF('Person'), $rdf.sym(locationurl)));

      deletions.push($rdf.st(file, RNSOC('hasRateValue'), $rdf.lit("3", '', XSD('int')), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file, RNSOC('respondedAt'), $rdf.lit("2020-04-01T15:18:12.948Z", '', XSD('dateTime')), $rdf.sym(locationurl)));

      deletions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));



      // store.statementsMatching(me, sym('http://xmlns.com/foaf/0.1/nick'), Literal(nickname), me.doc());

      console.log("deletions", deletions);



      let createAndUpdateTheFileObject = {
        location: locationurl,
        additions: additions,
        deletions: deletions
      };


      updateTheFileWithTriples(createAndUpdateTheFileObject.location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then((success) => {
        console.log("It is created");
      }).catch((error) => {
        console.log(error);
      });

    },
    delete_response_file_from_the_rnex_pod: function() {

      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = "";
      const location_url_of_renarration_with_id = locationurl + "#asd1256";
      const profile_card_me = $rdf.sym("");
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();
      let blanknodes = [];

      let file1 = $rdf.sym("");
      let file = $rdf.sym(location_url_of_renarration_with_id);
      deletions.push($rdf.st(file1, RDF('type'), OWL('NamedIndividual'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file1, RDF('type'), RNSOC('Renarration'), $rdf.sym(locationurl)));

      deletions.push($rdf.st(file, RDF('type'), RNSOC('Comment'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file, RNSOC('isCreatedBy'), profile_card_me, $rdf.sym(locationurl)));

      deletions.push($rdf.st(profile_card_me, RDF('type'), RNSOC('Renarrator'), $rdf.sym(locationurl)));
      deletions.push($rdf.st(profile_card_me, RDF('type'), FOAF('Person'), $rdf.sym(locationurl)));

      // additions.push($rdf.st(file, RNSOC('hasRateValue'), $rdf.lit("3", '', XSD('int')), $rdf.sym(locationurl)));
      deletions.push($rdf.st(file, RNSOC('respondedAt'), $rdf.lit("2020-03-01T15:18:12.948Z", '', XSD('dateTime')), $rdf.sym(locationurl)));

      deletions.push($rdf.st(file, RNSOC('responseOnRenarration'), file1, $rdf.sym(locationurl)));


      console.log("deletions", deletions);


      let createAndUpdateTheFileObject = {
        location: locationurl,
        additions: additions,
        deletions: deletions
      };



      updateTheFileWithTriples(createAndUpdateTheFileObject.location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then((success) => {
        console.log("It is created");
      }).catch((error) => {
        console.log(error);
      });
    },
    getTheResponseContentOfChosenNumberOfResponseTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod: async function(number_of_last_renarrations, desiredOnSourceDocument) {
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);
      let url_of_rnex_pod_list_file = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfResponseListInTheSolidPodOfRnEx;

      let renarration_array_url_list = await SolidPodOperations.response_turtle_operations.getTheResponseURLSOfChosenNumberOfResponseTurtleFilesFromTheRNExPod(number_of_last_renarrations, url_of_rnex_pod_list_file, desiredOnSourceDocument).then(s => {
        return s.slice();
      }).catch(e => console.log("error read contains renarration files", e));

      console.log("renarration_array_url_list", renarration_array_url_list);

      const renarrationReadPromise = new Promise((resolve, reject) => {

        fetcher.load(renarration_array_url_list).then(res => {

          console.log("res", res);
          let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
          let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
          let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
          let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
          let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
          let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
          let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
          let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
          let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");

          let response_document_list = [];

          let test01 = store.match(null, RDF('type'), RNSOC('Rate'), null).map(st => {
            let response_document = {};

            response_document.type = "Rate";
            response_document.id = st.subject.value;

            store.match($rdf.sym(st.subject.value), RNSOC('hasRateValue'), null, null).map(st3 => {
              response_document.hasRateValue = st3.object.value;
            });

            store.match($rdf.sym(st.subject.value), RNSOC('isCreatedBy'), null, null).map(st3 => {
              response_document.isCreatedBy = st3.object.value;
            });

            store.match($rdf.sym(st.subject.value), RNSOC('respondedAt'), null, null).map(st4 => {
              response_document.respondedAt = st4.object.value;
            });
            store.match($rdf.sym(st.subject.value), RNSOC('responseOnRenarration'), null, null).map(st4 => {
              response_document.responseOnRenarration = st4.object.value;
            });


            response_document_list.push(response_document);

          });

          let test0 = store.match(null, RDF('type'), RNSOC('Comment'), null).map(st => {
            let response_document = {};

            response_document.type = "Comment";
            response_document.id = st.subject.value;

            store.match($rdf.sym(st.subject.value), RNSOC('hasCommentValue'), null, null).map(st3 => {
              response_document.hasCommentValue = st3.object.value;
            });

            store.match($rdf.sym(st.subject.value), RNSOC('isCreatedBy'), null, null).map(st3 => {
              response_document.isCreatedBy = st3.object.value;
            });

            store.match($rdf.sym(st.subject.value), RNSOC('respondedAt'), null, null).map(st4 => {
              response_document.respondedAt = st4.object.value;
            });

            store.match($rdf.sym(st.subject.value), RNSOC('responseOnRenarration'), null, null).map(st4 => {
              response_document.responseOnRenarration = st4.object.value;
            });

            response_document_list.push(response_document);

          });

          console.log("response_document_list", response_document_list);
          resolve(response_document_list);

        }).catch(err => {
          console.log("failed fetcher load", err);
          reject(err);
        });

      });

      return await renarrationReadPromise;
    },
    getTheResponseURLSOfChosenNumberOfResponseTurtleFilesFromTheRNExPod: async function(number_of_last_responses, url_of_rnex_pod_list_file, desiredOnSourceDocument) {
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const renarrationReadPromise = new Promise((resolve, reject) => {

        fetcher.load(url_of_rnex_pod_list_file).then(res => {
          let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
          let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
          let RN = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
          let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
          let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
          let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
          let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
          let LDP = $rdf.Namespace("http://www.w3.org/ns/ldp#");
          let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
          let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

          let response_document_list = [];
          let desired_array_of_responses = [];

          let test01 = store.match(null, RDF('type'), AS('Create'), null).map(st => {
            let response_document = {};

            store.match($rdf.blankNode(st.subject.value), AS('object'), null, null).map(st3 => {
              response_document.id = st3.object.value;
            });
            store.match($rdf.blankNode(st.subject.value), AS('actor'), null, null).map(st3 => {
              response_document.isCreatedBy = st3.object.value;
            });

            store.match($rdf.blankNode(st.subject.value), AS('published'), null, null).map(st4 => {
              response_document.respondedAt = st4.object.value;
            });

            response_document_list.push(response_document);

          });

          response_document_list.forEach(responseActivityPart => {
            store.match($rdf.sym(responseActivityPart.id), RNSOC('responseOnRenarration'), null, null).map(st => {
              responseActivityPart.responseOnRenarration = st.object.value;
            });
            store.match($rdf.sym(responseActivityPart.id), RDF('type'), null, null).map(st => {
              responseActivityPart.type = st.object.value;
            });
          });


          let filtered_response_document_list = [];

          response_document_list.forEach((item, i) => {
            if (item.responseOnRenarration === desiredOnSourceDocument) {
              filtered_response_document_list.push(item);
            }


          });
          if (RenarrationSolidVariables.getAllTheResponsesForTheNotification === desiredOnSourceDocument) {
            filtered_response_document_list = response_document_list.slice();
          }

          let desired_number_of_renarrations = filtered_response_document_list.sort((a, b) => new Date(b.respondedAt) - new Date(a.respondedAt)).slice(0, number_of_last_responses);
          console.log("desired_number_of_renarrations", desired_number_of_renarrations);
          let response_file_with_duplicate_url = [];
          let response_files_with_unique_urls = [];
          desired_number_of_renarrations.forEach(response_obj => {

            let regex = /^.*?(?=\.ttl#)/g;
            let val = response_obj.id.match(regex);
            let url = val + ".ttl";
            response_file_with_duplicate_url.push(url);
            response_files_with_unique_urls = [...new Set(response_file_with_duplicate_url)];
          });
          console.log("response_files_with_unique_urls", response_files_with_unique_urls);

          if (RenarrationSolidVariables.getAllTheResponsesForTheNotification === desiredOnSourceDocument) {
            response_files_with_unique_urls = desired_number_of_renarrations.slice();
          }

          resolve(response_files_with_unique_urls);

        }).catch(err => {
          console.log("failed fetcher load", err);
          reject(err);
        });

      });

      return await renarrationReadPromise;
    }
  },
  follow_turtle_operations: {
    follow_the_renarrator: function(currentTime, location_of_following_list_file, renarratorsProfileCard, renarratorsProfileCardWhoWillBeFollowed) {


      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = location_of_following_list_file;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      const profile_card_of_the_renarrator_who_will_be_followed = $rdf.sym(renarratorsProfileCardWhoWillBeFollowed);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
      let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

      let blanknode1 = $rdf.blankNode();

      additions.push($rdf.st(blanknode1, RDF('type'), AS('Follow'), $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('actor'), profile_card_me, $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('object'), profile_card_of_the_renarrator_who_will_be_followed, $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('published'), $rdf.lit(currentTime, '', XSD('dateTime')), $rdf.sym(locationurl)));




      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;



    },
    getTheFollowingListFromTheRenarratorsPod: async function(locationOfTheFollowingList) {
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const renarrationReadPromise = new Promise((resolve, reject) => {

        fetcher.load(locationOfTheFollowingList).then(res => {

          let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
          let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");
          let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");

          let following_document_list = [];

          store.match(null, RDF('type'), AS('Follow'), null).map(st => {
            let following_document = {};

            store.match($rdf.blankNode(st.subject.value), AS('actor'), null, null).map(st3 => {
              following_document.actor = st3.object.value;
            });

            store.match($rdf.blankNode(st.subject.value), AS('object'), null, null).map(st3 => {
              following_document.object = st3.object.value;
            });

            store.match($rdf.blankNode(st.subject.value), AS('published'), null, null).map(st4 => {
              following_document.published = st4.object.value;
            });

            following_document_list.push(following_document);

          });

          resolve(following_document_list);
        }).catch(e => reject("error ", e));

      });

      return await renarrationReadPromise;

    }
  },
  notification_turtle_operations: {
    updateLastViewTimeForNotificationToRenarratorsPod: function(nameOfActivity, currentTime, renarratorsProfileCard, locationOfTheNotificationFile) {


      let store = $rdf.graph();
      const additions = [];
      const deletions = [];
      const locationurl = locationOfTheNotificationFile;
      const profile_card_me = $rdf.sym(renarratorsProfileCard.webId);
      let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
      let RNSOC = $rdf.Namespace(RenarrationSolidVariables.UrlOfTheRenarrationSocialOntology);
      let CNT = $rdf.Namespace("http://www.w3.org/2011/content#");
      let PURL = $rdf.Namespace("http://purl.org/dc/elements/1.1/");
      let FOAF = $rdf.Namespace("http://xmlns.com/foaf/0.1/");
      let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");
      let OWL = $rdf.Namespace("http://www.w3.org/2002/07/owl#");
      let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");

      let blanknode1 = $rdf.blankNode();
      let blanknode2 = $rdf.blankNode();

      additions.push($rdf.st(blanknode1, RDF('type'), AS('View'), $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('actor'), profile_card_me, $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('object'), blanknode2, $rdf.sym(locationurl)));
      additions.push($rdf.st(blanknode2, AS('name'), $rdf.lit(nameOfActivity, '', XSD('string')), $rdf.sym(locationurl)));

      additions.push($rdf.st(blanknode1, AS('published'), $rdf.lit(currentTime, '', XSD('dateTime')), $rdf.sym(locationurl)));




      console.log("additions", additions);


      let createAndUpdateTheFileObject = {
        response_location: locationurl,
        additions: additions,
        deletions: deletions
      };

      return createAndUpdateTheFileObject;


    },
    readLastViewTimeOfNotificationFromRenarratorsPod: async function(locationOfTheNotificationFile) {
      const store = $rdf.graph();
      const fetcher = new $rdf.Fetcher(store);

      const renarrationReadPromise = new Promise((resolve, reject) => {

        fetcher.load(locationOfTheNotificationFile).then(res => {

          let RDF = $rdf.Namespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#");
          let AS = $rdf.Namespace("https://www.w3.org/ns/activitystreams#");
          let XSD = $rdf.Namespace("http://www.w3.org/2001/XMLSchema#");

          let last_view_notification_document_list = [];

          store.match(null, RDF('type'), AS('View'), null).map(st => {
            let last_view_notification_document = {};

            store.match($rdf.blankNode(st.subject.value), AS('actor'), null, null).map(st3 => {
              last_view_notification_document.actor = st3.object.value;
            });

            store.match($rdf.blankNode(st.subject.value), AS('object'), null, null).map(st3 => {
              store.match($rdf.blankNode(st3.object.value), AS('name'), null, null).map(st4 => {
                last_view_notification_document.extensionActivityName = st4.object.value;
              });


            });

            store.match($rdf.blankNode(st.subject.value), AS('published'), null, null).map(st4 => {
              last_view_notification_document.published = st4.object.value;
            });

            last_view_notification_document_list.push(last_view_notification_document);

          });

          resolve(last_view_notification_document_list);
        }).catch(e => resolve(null));

      });

      return await renarrationReadPromise;
    }
  },
  get_the_renarrator_file_url_and_return_response_file_url_ending: function(renarration_location) {
    let regex = /(http[s]?:\/\/)?([^\/\s]+)/g;
    let val = renarration_location.match(regex);

    let regex_for_dash_part = /(-)\S+/g;
    let val_after_dash = val[3].match(regex_for_dash_part);

    let response_file = RenarrationSolidVariables.locationOfResponseFolderInTheSolidPodOfUserRSPCNTTO + val_after_dash;

    let regex2 = /^.*?(?=\.ttl#)/g;
    let val2 = response_file.match(regex2);
    let response_file2 = val2 + ".ttl";

    let response_file_ending = "/" + RenarrationSolidMainFolderVariables.podMainRenarrationFolder + "/" + RenarrationSolidMainFolderVariables.podResponseFolderPath + "/" + response_file2;

    return response_file_ending;
  }
};


chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
  let localStorageSolidAuthClient;

  if (localStorage['solid-auth-client']) {
    localStorageSolidAuthClient = JSON.parse(localStorage['solid-auth-client']);
  }
  if (request.messageType === "rn_tracksession") {

    let localStorageWebId = SolidPodOperations.solidTrackSessionWithLocalStorage();
    if (localStorageWebId !== null && localStorageWebId !== undefined) {
      SolidPodOperations.getTheProfileInfo(localStorageWebId.webId).then(success => {
        chrome_storage.rn_set_the_current_session_renarrators_info(success);
      });

      sendResponse([{
        body: localStorageWebId
      }, null]);
    } else {
      sendResponse([null, {
        error: "localStorageWebId does not exist error"
      }]);
    }

  }


  if (localStorageSolidAuthClient && (localStorageSolidAuthClient.session || localStorageSolidAuthClient.rpConfig)) {
    let RenarratorWebIdObject = {};
    if (localStorageSolidAuthClient.session) {
      let webIdWithAndWithoutProfileCard = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(localStorageSolidAuthClient.session.webId);
      let webIdWithoutProfileCard = webIdWithAndWithoutProfileCard[0];
      RenarratorWebIdObject = {
        webIdWithoutProfileCard: webIdWithoutProfileCard,
        webId: localStorageSolidAuthClient.session.webId,
        idp: localStorageSolidAuthClient.session.idp
      };
    } else if (localStorageSolidAuthClient.rpConfig) {
      let webIdWithAndWithoutProfileCard = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(localStorageSolidAuthClient.rpConfig.provider.url);
      let webIdWithoutProfileCard = webIdWithAndWithoutProfileCard[0];
      RenarratorWebIdObject = {
        webIdWithoutProfileCard: webIdWithoutProfileCard,
        webId: webIdWithAndWithoutProfileCard[1],
        idp: localStorageSolidAuthClient.rpConfig.provider.configuration.issuer
      };
    }

    chrome_storage.rn_set_the_session_info(RenarratorWebIdObject);
    //  SolidPodOperations.initialCreateRenarrationFolderAndFiles(RenarratorWebIdObject);
    if (request.messageType === "rn_logout") {
      SolidPodOperations.solidLogout().then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else if (request.messageType === "rn_update_renarration_to_renarrators_and_rnex_pod") {

      let renarrationLocation = RenarratorWebIdObject.webIdWithoutProfileCard + RenarrationSolidVariables.locationOfRenarrationFolderInTheSolidPodOfUser + RenarrationSolidVariables.locationOfRenarrationFolderInTheSolidPodOfUserRNCNT + request.renarration_unique_id + RenarrationSolidVariables.renarrationFileType;

      request.renarration_information_send_object.webId = RenarratorWebIdObject.webIdWithoutProfileCard;
      let createAndUpdateTheFileObject = SolidPodOperations.renarration_turtle_operations(renarrationLocation, request.renarration_information_send_object, request.renarration_transformation_send_object);

      let createAndUpdateTheRnExListFileObject = SolidPodOperations.renarrationlist_creation_inside_rnex_pod_turtle_operations(renarrationLocation, request.renarration_information_send_object);

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheFileObject.location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheRnExListFileObject.location, createAndUpdateTheRnExListFileObject.additions, createAndUpdateTheRnExListFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else if (request.messageType === "rn_get_the_renarrations_from_the_renarrators_pod") {

      let locationOfTheRenarrationFiles = RenarratorWebIdObject.webIdWithoutProfileCard + RenarrationSolidVariables.locationOfRenarrationFolderInTheSolidPodOfUser;
      let table_footer_action_obj = request.messageValues;
      SolidPodOperations.getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod(RenarrationSolidVariables.limitOfRenarrationsThatWillBeGotFromRenarratorsPod, table_footer_action_obj, locationOfTheRenarrationFiles).then(success => {
        console.log("success getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod", success);

        chrome_storage.rn_set_the_renarrations_and_the_table_for_the_renarrator_account_mode(success).then(
          (res) => {
            sendResponse([{
              renarrationArray: success
            }, null]);
          }
        );

      }).catch(error => {
        console.log("error getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod", error);
        sendResponse([null, error]);
      });



    } else if (request.messageType === "rn_get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod") {

      console.log("request rn_get_the_renarrations_from_the_rnex_then_from_the_renarrators_pod renarration_information", request.messageValues);

      let desiredOnSourceDocument = request.messageValues.desiredOnSourceDocument;
      let desiredRenarratedBy = request.messageValues.desiredRenarratedBy;
      let currentMode = request.messageValues.currentMode;
      let url_of_the_rnex_renarration_list = RenarrationSolidVariables.urlOfRenarrationExtensionPod + RenarrationSolidVariables.locationOfRenarrationListInTheSolidPodOfRnEx;
      let table_footer_action_obj = request.messageValues.tableAction;
      SolidPodOperations.getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPodWithInformationFromRnExCentralPod(RenarrationSolidVariables.limitOfRenarrationsThatWillBeGotForTheReadingMode, url_of_the_rnex_renarration_list, table_footer_action_obj, desiredOnSourceDocument, desiredRenarratedBy).then(success => {
        console.log("success getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod", success);

        if (currentMode === "reading_mode") {
          chrome_storage.rn_set_the_renarrations_and_the_table_for_the_reading_mode(success);
        } else if (currentMode === "profile_mode") {
          chrome_storage.rn_set_the_renarrations_and_the_table_for_the_profile_mode(success);
        }

        sendResponse([{
          renarrationArray: success
        }, null]);
      }).catch(error => {
        console.log("error getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod", error);
        sendResponse([null, error]);
      });

      if (currentMode === "profile_mode") {
        SolidPodOperations.getTheProfileInfo(desiredRenarratedBy).then(success => {
          return chrome_storage.rn_set_the_renarrators_info(success);
        }).catch(error => {
          console.log("error the visited renarrators info can't be set", error);
        });
      }

    } else if (request.messageType === "rn_get_the_renarrations_from_the_other_renarrators_pod") {

      console.log("request rn_get_the_renarrations_from_the_other_renarrators_pod renarration_information", request.messageValues);

      let locationOfTheRenarrationFiles = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(request.messageValues.renarration_unique_id_with_pod_location)[0] + RenarrationSolidVariables.locationOfRenarrationFolderInTheSolidPodOfUser;

      SolidPodOperations.getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromTheOtherRenarratorsPod(RenarrationSolidVariables.limitOfRenarrationsThatWillBeGotFromOtherRenarratorsPod, locationOfTheRenarrationFiles).then(success => {
        console.log("success getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesFromAPod", success);


        chrome_storage.rn_set_the_renarration_documents_array_for_the_renarrator_profile_mode(success).then(
          (res) => {
            console.log(res);
            sendResponse([{
              renarrationArray: success
            }, null]);
          }
        ).catch(e => {
          console.log(e);
        });

      }).catch(error => {
        console.log("error getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod", error);
        sendResponse([null, error]);
      });

      SolidPodOperations.getTheProfileInfo(request.messageValues.renarration_unique_id_with_pod_location).then(success => {
        console.log("success ", success);
        return chrome_storage.rn_set_the_renarrators_info(success);
      }).then(success => {
        console.log("success", success);
      }).catch(error => {
        console.log("error", error);
      });

    } else if (request.messageType === "rn_response_mode_update_comment_response_to_renarrators_pod") {


      let responseLocation = SolidPodOperations.get_the_renarrator_file_url_and_return_response_file_url_ending(request.messageValues.renarration_location);


      let createAndUpdateTheFileObject = SolidPodOperations.response_turtle_operations.create_response_comment_file_on_the_renarrators_pod(RenarratorWebIdObject, request.messageValues.renarration_location, responseLocation, request.messageValues.comment, request.messageValues.comment_information);

      let createAndUpdateTheRnExListFileObject = SolidPodOperations.response_turtle_operations.create_response_comment_file_on_the_rnex_pod(RenarratorWebIdObject, request.messageValues.renarration_location, responseLocation, request.messageValues.comment, request.messageValues.comment_information);

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheFileObject.response_location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheRnExListFileObject.response_location, createAndUpdateTheRnExListFileObject.additions, createAndUpdateTheRnExListFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else if (request.messageType === "rn_response_mode_update_rate_response_to_renarrators_pod") {

      let responseLocation = SolidPodOperations.get_the_renarrator_file_url_and_return_response_file_url_ending(request.messageValues.renarration_location);


      let createAndUpdateTheFileObject = SolidPodOperations.response_turtle_operations.create_response_rate_file_on_the_renarrators_pod(RenarratorWebIdObject, request.messageValues.renarration_location, responseLocation, request.messageValues.rate, request.messageValues.rate_information);

      let createAndUpdateTheRnExListFileObject = SolidPodOperations.response_turtle_operations.create_response_rate_file_on_the_rnex_pod(RenarratorWebIdObject, request.messageValues.renarration_location, responseLocation, request.messageValues.rate, request.messageValues.rate_information);

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheFileObject.response_location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

      SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheRnExListFileObject.response_location, createAndUpdateTheRnExListFileObject.additions, createAndUpdateTheRnExListFileObject.deletions).then(success => {
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else if (request.messageType === "rn_get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod") {

      console.log("request rn_get_the_responses_from_the_rnex_and_then_from_the_renarrators_pod renarration_information", request.messageValues);



      SolidPodOperations.response_turtle_operations.getTheResponseContentOfChosenNumberOfResponseTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod(RenarrationSolidVariables.limitOfRenarrationsThatWillBeGotForTheResponseMode, request.messageValues.desiredOnSourceDocument).then(success => {
        console.log("success getTheResponseContentOfChosenNumberOfResponseTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod", success);

        chrome_storage.rn_set_response_mode_response_array(success);

        sendResponse([{
          responseArray: success
        }, null]);

      }).catch(error => {
        console.log("error getTheRenarrationContentOfChosenNumberOfRenarrationTurtleFilesUsingTheInformationFromRNExPodAndGettingTheContentFromRenarratorsPod", error);
        sendResponse([null, error]);
      });



    } else if (request.messageType === "rn_get_the_notifications_from_the_rnex_pod") {
      let readLastViewTimeNotification = null;
      let nameOfActivity = RenarrationSolidVariables.objectExtensionActivityLastViewTimeOfNotification;
      let webIdWithoutProfileCard = RenarratorWebIdObject.webIdWithoutProfileCard;

      let locationOfTheNotificationFile = RenarrationSolidVariables.locationOfExtensionActivitiesListInTheSolidPodOfUser;

      let locationOfFollowingListInTheSolidPodOfUser = RenarrationSolidVariables.locationOfFollowingListInTheSolidPodOfUser;

      SolidPodOperations.notification_turtle_operations.readLastViewTimeOfNotificationFromRenarratorsPod(webIdWithoutProfileCard + locationOfTheNotificationFile).then(
        (last_view_notification_document) => {
          if (last_view_notification_document && last_view_notification_document.length > 0) {
            let result = last_view_notification_document.sort((a, b) => new Date(b.published) - new Date(a.published));

            let lasViewTimeValueOfNotification = result[0].published;
            readLastViewTimeNotification = lasViewTimeValueOfNotification;
          }
          console.log("first", readLastViewTimeNotification);
          let currentTime = new Date().toISOString();


          let createAndUpdateTheFileObject = SolidPodOperations.notification_turtle_operations.updateLastViewTimeForNotificationToRenarratorsPod(nameOfActivity, currentTime, RenarratorWebIdObject, webIdWithoutProfileCard + locationOfTheNotificationFile);

          return SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheFileObject.response_location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions);
        }
      ).finally(() => {

        console.log("first", readLastViewTimeNotification);

        SolidPodOperations.follow_turtle_operations.getTheFollowingListFromTheRenarratorsPod(webIdWithoutProfileCard + locationOfFollowingListInTheSolidPodOfUser).then(followingList => {
          return SolidPodOperations.getTheNotificationsFromTheRnexPod(readLastViewTimeNotification, followingList);
        }).then(ListOfNotifications => {
          console.log("ListOfNotifications", ListOfNotifications);
          chrome_storage.rn_set_notification_mode_notification_array(ListOfNotifications);
        });
      });



    } else if (request.messageType === "rn_get_the_recommendations_from_the_rnex_pod") {

      SolidPodOperations.recommending_turtle_operations.giveTheListOfRenarratorsByGettingResponseItems(RenarratorWebIdObject.webId).then(
        (renarration) => {
          chrome_storage.rn_set_recommending_mode_recommendation_depending_on_second_degree_response_array(renarration);
          console.log('success', renarration);
        }
      );

    } else if (request.messageType === "rn_get_the_search_result_from_the_rnex_pod") {
      SolidPodOperations.recommending_turtle_operations.getTheRenarrationsFromRnExPodAndApplySparqlQuery().then(renarenarrations => {
        console.log("getTheRenarrationsFromRnExPodAndApplySparqlQuery success", renarenarrations);
        chrome_storage.rn_set_recommending_mode_search_operation_all_the_renarrations_array(renarenarrations);
      });

    } else if (request.messageType === "rn_get_the_renarration_from_the_url") {
      console.log("request.messageValues.renarrationUniqueUrl", request.messageValues.renarrationUniqueUrl);
      let renarrationUniqueUrlArray = [request.messageValues.renarrationUniqueUrl];
      SolidPodOperations.getTheRenarrationsFromARenarratorsPodCommonFunction(renarrationUniqueUrlArray).then(success => {
        console.log("request.messageValues.renarrationUniqueUrl success", success);
        chrome_storage.rn_set_recommending_mode_chosen_renarration(null).then(() => {
          chrome_storage.rn_set_recommending_mode_chosen_renarration(success);
        });
      });

    } else if (request.messageType === "rn_get_the_searched_renarration_result_from_the_rnex_pod") {

      SolidPodOperations.recommending_turtle_operations.getTheSearchedRenarrationsFromARNExPodWithSPARQL(request.messageValues.operation_type, request.messageValues.text_value).then(renarrations => {
        console.log("request.messageValues.renarrationUniqueUrl success", renarrations);
        chrome_storage.rn_set_recommending_mode_search_operation_filtered_rnex_renarrations_array(null).then(() => {
          chrome_storage.rn_set_recommending_mode_search_operation_filtered_rnex_renarrations_array(renarrations);
        });

      });


    } else if (request.messageType === "rn_follow_the_renarrator") {

      let get_the_renarrator_webId_from_the_url_with_regex = SolidPodOperations.getTheWebIdFromTheUrlWithRegex(request.messageValues.renarratorWebId);
      let renarrator_profile_card = get_the_renarrator_webId_from_the_url_with_regex[1];
      let renarrator_webId = get_the_renarrator_webId_from_the_url_with_regex[0];
      let renarratorsProfileCardWhoWillBeFollowed = renarrator_profile_card;
      if (RenarratorWebIdObject.webId !== renarrator_profile_card) {
        let currentTime = new Date().toISOString();
        let createAndUpdateTheFileObject = SolidPodOperations.follow_turtle_operations.follow_the_renarrator(currentTime, RenarratorWebIdObject.webIdWithoutProfileCard + RenarrationSolidVariables.locationOfFollowingListInTheSolidPodOfUser, RenarratorWebIdObject, renarratorsProfileCardWhoWillBeFollowed);

        SolidPodOperations.checkCreateAndUpdateTheFile(RenarratorWebIdObject, createAndUpdateTheFileObject.response_location, createAndUpdateTheFileObject.additions, createAndUpdateTheFileObject.deletions).then(success => {
          console.log("succcess follow_turtle_operations ");
          sendResponse([{
            body: success
          }, null]);
        }).catch(error => {
          sendResponse([null, error]);
        });
      } else {
        console.log("You can't follow your profile");
      }

    } else if (request.messageType === "rn_send_renarration_or_response_to_text_analytics") {

      let textToAnalyze = request.messageValues.textToAnalyze;
      console.log(textToAnalyze);

      ExternalAPIOperations.sendTheTextToExternalAPI(textToAnalyze).then(success => {
        chrome_storage.rn_set_external_api_analytics_for_the_applied_renarration_on_the_page(success);
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else if (request.messageType === "rn_delete_the_renarration") {
      let url_of_renarration_for_deletion = request.messageValues.url_of_renarration_for_deletion;
      SolidPodOperations.deleteTheFile(url_of_renarration_for_deletion).then(success => {
        console.log(`The file ${url_of_renarration_for_deletion} is deleted`);
        sendResponse([{
          body: success
        }, null]);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else {
      console.log("backgroundjs there is no message type");
    }

  } else {
    console.log("There is no session, you should login");

    if (request.messageType === "rn_login") {

      SolidPodOperations.solidLogin().then(success1 => {
        return SolidPodOperations.solidlogin3();

      }).then(success => {

        sendResponse([{
          body: success
        }, null]);

        let localStorageWebId = SolidPodOperations.solidTrackSessionWithLocalStorage();
        return SolidPodOperations.getTheProfileInfo(localStorageWebId.webId);
      }).then(success => {
        chrome_storage.rn_set_the_current_session_renarrators_info(success);
      }).catch(error => {
        sendResponse([null, error]);
      });

    } else {
      if (firstActiveTab) {
        chrome.tabs.sendMessage(firstActiveTab.id, {
          "message": "no_session"
        });
      }

    }
  }

  return true;
});
