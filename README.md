**RNEx - A decentralized social renarration platform**


- The master's thesis of [Accessibility On The Web Through Semantic And Social
Renarrations](https://gitlab.com/neua/RNEx/-/wikis/uploads/b48813cd1adda827ddf8f1aa435b305f/10385284.pdf) contains a description for the RNEx and its usage.



- The [Renarration Social Ontology](http://soslab.cmpe.boun.edu.tr/ontologies/rnsoc.owl) is an ontology that is introduced in the thesis to specify
the concepts and properties of renarrations with social interactions.


- The user manual for the RNEx can be found on the following link [RNEx-User-Manual.pdf](https://gitlab.com/neua/RNEx/-/wikis/uploads/cef58ec5b448550ca73dba71810e2dd7/RNEx-User-Manual.pdf)

**Demonstration**


- A demonstration of RNEx can be seen in the below.

<img src="https://drive.google.com/uc?id=1wUNBZvepsVsE3luBenI-ZEzlsCU0Rnvl" alt="RNEx Demonstration" title="RNEx Demonstration" width="1080"/>


